import frida
import json
import sys

rpc_sign = """
rpc.exports = {
    getsign: function(function_id, body_string, uuid,clientVersion){
      var sig = "";
      Java.perform(
        function(){
            //拿到context上下文
            var currentApplication = Java.use('android.app.ActivityThread').currentApplication();
            var context = currentApplication.getApplicationContext();
            var BitmapkitUtils = Java.use('com.jingdong.common.utils.BitmapkitUtils');
            sig = BitmapkitUtils.getSignFromJni(context, function_id, body_string, uuid, 'android', clientVersion);
        } 
      )
       return sig;
    }
};
"""


def get_sign(function_id, body_string, u,clientVersion):
    process = frida.get_remote_device().attach('com.jingdong.app.mall')
    script = process.create_script(rpc_sign)
    script.load()
    sign = script.exports.getsign(function_id, body_string, u,clientVersion)
    return sign


def get_sign_result(function_id, body_data, uuid):
    body_string = json.dumps(body_data, ensure_ascii=False).replace(" ", "")
    sign = get_sign(function_id, body_string, uuid)
    return sign


body_data = {"isShowCode": "0", "skuId": "10025739731561", "sr": "1"}
body_data ={"CartStr":{"TheSkus":[{"Id":"100009958327","num":"1"}]},"OrderStr":{"CoordType":"2","Email":"","Id":707695531,"IdArea":49324,"IdCity":1930,"IdProvince":22,"IdTown":49399,"Latitude":"30.531668","Longitude":"104.095993","Mobile":"uUCGVagccJ4hDz1DCsH4GA==","Name":"242aBN3YnQU=","Phone":"","Where":"O1YzJh1E3pGXyO8ewxJeCpfzP0tscYwFoXZlfVIrft18/uhC/4d2P4e/jqhd8hxoLddQDHf4OpnKanKcu9hKSMF3rUeunqmH","addressDefault":True,"addressDetail":"2OlaL+DUec5HhMhuI5ZgYSj/wlcMMCH8TuKjttJaAA2JRMeXQ6yEIg==","addressUUID":"1011_910518214399463429","areaCode":"","bubbleTimeInfo":{"shipmentInfos":1610266527095},"closedTimeStamp":"","closedVersion":"","isNeedVirtualData":True,"isOpenPaymentPassword":True,"isShortPwd":False,"latitude":"","longitude":"","plusExposureCount":6,"plusFloorClosedTimeStamp":"1604749322501","postCode":"","redpacket_channel":"JD","retainTipsNumber":0},"addressGlobal":True,"addressTotalNum":3,"appEID":"eidA173d812283saTEW2oEfoSsm7EyVu0iQwgkxFuBL+FRL/ypxaKNgzQ7RTrvjE1AF38mZMvZVIBVz9uvg1qDX7hqrAhSpKlZC21+GerZIt5g0pC3s2","cartAdd":{"atmosphereList":[{"necessaryKey":"10","showMsg":"3000元以上旗舰手机热卖榜第5名","type":4},{"necessaryKey":"91%","type":5}],"extFlag":{},"wareId":"100009958327","wareNum":"1"},"decryptType":"2","giftType":0,"hasDefaultAddress":True,"hasSelectedOTC":"0","isLastOrder":True,"isRefreshOrder":False,"isSupportAllInvoice":True,"operationType":0,"otcMergeSwitch":"1","settlementVersionCode":1240,"skuSource":3,"sourceType":2,"supportAllEncode":True,"wareId":"100009958327","wareNum":1}
body_string = json.dumps(body_data, ensure_ascii=False).replace(" ", "")
loads = json.loads(sys.argv[3])
body_string1 = json.dumps(loads, ensure_ascii=False).replace(" ", "")
function_id = 'currentOrder'
uuid = 'f5d23d2a30f03be8'
sign = get_sign(sys.argv[1], body_string1, sys.argv[2],sys.argv[4])
# sign = get_sign(function_id, uuid, )
print(sign)

