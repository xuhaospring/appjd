# -*- coding:utf-8 -*-
from selenium import webdriver
import PIL.Image as image
import time, re, random
import requests
from selenium.webdriver import ActionChains
import json


# cpc_img_sf = cpc_img[22:]
# print(cpc_img_sf)
# print("这里输出的数据为base64的数据")
# m = hashlib.md5()
# m.update(cpc_img_sf.encode())
# md5_img = m.hexdigest()
# print(md5_img)
#
# headers = {'Encoding': "UTF8",
#            'Origin': "http://www.jd.con",
#            'Referer': "plogin.m.jd.com",
#            'User-Agent': "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36", }
# # http://laiket.cn:7777/get?user=2082914704&pwd=hc20021122&type=10001&do=get&jd_user=fyjxmAsAn
# # http://laiket.cn:7777/get?user=lai_te_ke_username&pwd=lai_te_ke_password&type=20002&do=send&picMd5=1d87572df9e120a2e75357ddebad2e3e
# upload = requests.post(
#     "http://laiket.cn:7777/get?user=2082914704&pwd=hc20021122&type=20002&do=send&picMd5=%s" % (md5_img),
#     data=cpc_img_sf, headers=headers)
# print("http://laiket.cn:7777/get?user=2082914704&pwd=hc20021122&type=20002&do=send&picMd5=%s" % (md5_img))
# time.sleep(3)
# print("http://laiket.cn:7777/get?user=2082914704&pwd=hc20021122&type=20002&do=get&picMD5=%s" % (md5_img))
# download = requests.get(
#     "http://laiket.cn:7777/get?user=2082914704&pwd=hc20021122&type=20002&do=get&picMD5=%s" % (md5_img),
#     headers=headers)
# download_str = download.text
# xy = []
# if "1001" in download_str:
#     # code: 1001, msg: 177, 93 | 42, 56 | 67, 116 | 170, 53
#     xy_list_str = download_str[14:]
#     xy = xy_list_str.split("|")
# x1 = float(xy[0].split(",")[0])
# x2 = float(xy[1].split(",")[0])
# x3 = float(xy[2].split(",")[0])
# x4 = float(xy[3].split(",")[0])
# y1 = float(xy[0].split(",")[1])
# y2 = float(xy[1].split(",")[1])
# y3 = float(xy[2].split(",")[1])
# y4 = float(xy[3].split(",")[1])

# x1 = float(20)
# x2 = float(200)
# x3 = float(80)
# x4 = float(10)
# y1 = float(10)
# y2 = float(100)
# y3 = float(130)
# y4 = float(70)

# 滑动验证码破解程序
def main():
    chrome_driver_path = r"D:\python\chromedriver_win32\chromedriver_win32_87\chromedriver.exe"
    # chrome_driver_path = r"E:\jd自动签到脚本\chromedriver_win32\chromedriver.exe"
    mobile_emulation = {
        "deviceMetrics": {"width": 360, "height": 640, "pixelRatio": 3.0},
        "userAgent": "Mozilla/5.0 (iPhone; CPU iPhone OS 13_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/89.0.4389.90 Mobile/15E148 Safari/604.1"}
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_experimental_option("mobileEmulation", mobile_emulation)
    chrome_options.add_argument("--auto-open-devtools-for-tabs")
    # chrome_options.add_argument('--headless')
    chrome_options.add_argument('--disable-gpu')  # 上面三行代码就是为了将Chrome不弹出界面，实现无界面爬取
    driver = webdriver.Chrome(options=chrome_options, executable_path=chrome_driver_path)
    driver.get("https://plogin.m.jd.com/register/mreg/breg")
    # 输入手机号码
    phone_data = requests.get("http://xuhao.vaiwan.com/sms/phone")
    phone_data_json = json.loads(phone_data.text)
    if phone_data_json['code'] == 0:
        phone_data_json_data = phone_data_json['data']
        phone_data_json_data_context = json.loads(json.dumps(phone_data_json_data))
        phone = phone_data_json_data_context['phone']
        username = phone_data_json_data_context['username']
        password = phone_data_json_data_context['password']
        driver.find_element_by_xpath("//input[@placeholder='请输入手机号码']").send_keys(phone)
        # 发送验证码
        driver.find_element_by_xpath("//button[@class='getMsg-btn timer active']").click()
        time.sleep(2)
        cpc_img = driver.find_element_by_id("cpc_img").get_attribute("src")
        laiteke_20002 = requests.post("http://xuhao.vaiwan.com/laiteke/20002", data=cpc_img)
        laiteke_20002_text = json.loads(laiteke_20002.text)
        print("获取滑动解锁的集合")
        x1, x_array, y1, y_array = laiteke_2020_array(laiteke_20002_text)
        cpc_img_element = driver.find_element_by_id("cpc_img")
        a = ActionChains(driver).move_to_element_with_offset(cpc_img_element, x1, y1).pause(1).click_and_hold().pause(1)
        for i in range(len(x_array) - 1):
            a.move_to_element_with_offset(cpc_img_element, x_array[i + 1], y_array[i + 1])
        a.release().perform()
        time.sleep(2)
        print("滑动解锁完成")
        driver.find_element_by_xpath("//button[@class='dialog-sure']").click()
        print("点击确定完成")
        code_data = requests.get("http://xuhao.vaiwan.com/sms/phone/%s" % phone)
        code_data_json = json.loads(code_data.text)
        code = ""
        if code_data_json['code'] == 0:
            code_data_json_data = code_data_json['data']
            phone_data_json_data_context = json.loads(json.dumps(code_data_json_data))
            code = phone_data_json_data_context['code']
            print("验证码" + code)

        driver.find_element_by_xpath("//input[@placeholder='请输入验证码']").send_keys(code)
        driver.find_element_by_xpath("//input[@placeholder='用作账号登录，设置后不可更改']").send_keys(username)
        driver.find_element_by_xpath("//input[@placeholder='请设置密码（8-20位）']").send_keys(password)
        driver.find_element_by_xpath("//i[@class='uncheck']").click()
        print("第一面成功")
        driver.find_element_by_xpath("//*[@id='app']/div/div[2]/a").click()
        time.sleep(10)
        companyName = "东莞市超利科技有限公司"
        taxpayer_id = "91441900MA54CAMD9U"
        driver.find_element_by_xpath("//input[@name='companyName']").send_keys(companyName)
        driver.find_element_by_xpath("//input[@name='taxpayerId']").send_keys(taxpayer_id)
        #
        driver.find_element_by_xpath("//*[@id='app']/div/div/div[3]/div[2]/button").click()
        print("注册成功")
        time.sleep(3)
        zuul_comtext = driver.find_element_by_xpath("//div[@class='text em']").text
        if "注册成功" in zuul_comtext:
            print("注册成功。账号是 :%s  密码是：%s" % (username, password))
        else:
            print("注册失败")
        time.sleep(1000)
    else:
        print("获取手机号失败")


def laiteke_2020_array(laiteke_20002_text):
    if laiteke_20002_text['code'] == 0:
        x1y1 = laiteke_20002_text['data']
        x1_y1 = json.loads(x1y1)
        x1 = float(x1_y1['x1'])
        x2 = float(x1_y1['x2'])
        x3 = float(x1_y1['x3'])
        x4 = float(x1_y1['x4'])
        y1 = float(x1_y1['y1'])
        y2 = float(x1_y1['y2'])
        y3 = float(x1_y1['y3'])
        y4 = float(x1_y1['y4'])
    # 直线斜率
    k1 = round((y1 - y2) / (x1 - x2), 3)
    k2 = round((y2 - y3) / (x2 - x3), 3)
    k3 = round((y3 - y4) / (x3 - x4), 3)
    # 截距
    b1 = round((y2 * x1 - y1 * x2) / (x1 - x2), 3)
    b2 = round((y3 * x2 - y2 * x3) / (x2 - x3), 3)
    b3 = round((y4 * x3 - y3 * x4) / (x3 - x4), 3)
    x_array = []
    y_array = []
    x_array.append(x1)
    y_array.append(y1)
    # 第一条
    if x1 - x2 < 0:
        b_size = int((x2 - x1) / 20)
        for i in range(b_size):
            x_t = x1 + 20 * i
            x_array.append(int(x_t))
            y_array.append(int(k1 * x_t + b1))
    else:
        b_size = int((x1 - x2) / 20)
        for i in range(b_size):
            x_t = x1 - (20 * i)
            x_array.append(int(x_t))
            y_array.append(int(k1 * x_t + b1))
    x_array.append(x2)
    y_array.append(y2)
    # 第二条
    if x2 - x3 < 0:
        b_size = int((x3 - x2) / 20)
        for i in range(b_size):
            x_t = x2 + 20 * i
            x_array.append(int(x_t))
            y_array.append(int(k2 * x_t + b2))
    else:
        b_size = int((x2 - x3) / 20)
        for i in range(b_size):
            x_t = x2 - (20 * i)
            x_array.append(int(x_t))
            y_array.append(int(k2 * x_t + b2))
    x_array.append(x3)
    y_array.append(y3)
    # 第三条
    if x3 - x4 < 0:
        b_size = int((x4 - x3) / 10)
        for i in range(b_size):
            x_t = x3 + 10 * i
            x_array.append(x_t)
            y_array.append(int(k3 * x_t + b3))
    else:
        b_size = int((x3 - x4) / 10)
        for i in range(b_size):
            x_t = x3 - (10 * i)
            x_array.append(int(x_t))
            y_array.append(int(k3 * x_t + b3))
    x_array.append(x4)
    y_array.append(y4)
    return x1, x_array, y1, y_array


# 主函数入口
if __name__ == "__main__":
    print(int("1"))
    pass
    # chrome_driver_path = r"E:\jd自动签到脚本\chromedriver.exe"
    # mobileEmulation = {'deviceName': 'iPhone 6'}
    # chrome_options = webdriver.ChromeOptions()
    # chrome_options.add_experimental_option('mobileEmulation', mobileEmulation)
    # driver = webdriver.Chrome(options=chrome_options, executable_path=chrome_driver_path)
    # driver.get('http://www.baidu.com')
    main()