package com.jd.mskill;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class MskillApplication {

    public static void main(String[] args) {
        SpringApplication.run(MskillApplication.class, args);
    }

}
