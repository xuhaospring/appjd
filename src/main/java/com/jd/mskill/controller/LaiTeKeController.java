package com.jd.mskill.controller;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.jd.mskill.config.CommonConstants;
import com.jd.mskill.config.LaiTeKe;
import com.jd.mskill.config.R;
import com.jd.mskill.mapper.SysImgMapper;
import com.jd.mskill.po.SysImg;
import com.jd.mskill.vo.X1y1x2y2x3y3x4y4;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Date;

@RestController
@Slf4j
@RequestMapping("/laiteke")
public class LaiTeKeController {

    @Autowired
    private LaiTeKe laiTeKe;

    @Resource
    private SysImgMapper sysImgMapper;

    public static void main(String[] args) {
        String a = "data:image/jpg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAgICAgQDAgICAgUEBAMEBgUGBgYFBgYGBwkIBgcJBwYGCAsICQoKCgoKBggLDAsKDAkKCgr/2wBDAQICAgICAgUDAwUKBwYHCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgr/wAARCACqARMDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4%2BTl5ufo6erx8vP09fb3%2BPn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3%2BPn6/9oADAMBAAIRAxEAPwD5etTrSzJI2kEsqnKrKT/Sq%2Br32pXZJOlSq7OWyZMqOvfOa7%2Bx8IQ6hZNLDdMkisY1gCnqM5wRwBn6jg1lz6bcaTKlvKtyVKn5%2Bq5Hbg8jv0wMV7kJrm0M8Rg6sYanl13oWqtqQnayYrkMc9s85%2BbIx711GhyGyvEkDspVTvKAEhe/XitrUJ7e/QxSMCF4DFcHPvxz6ZqKwsNIt5Bbyxl2dt8jkYB9v0rtjNcup85PANVbxkbfhXW3a7e7mIiZhtkVFJZlHIODx37Vty2xv5WdbViGlYg7TgKSWI559PzqvolzpRY/ZreIYA4t2JZfxziumSISqY4JFjIXIITBB6HJ7Hpnn0oWIjE6llM6q11OZl0mJI/KuRgE8%2BW2KxNQ8IW5lO2QqH475Ix14610uoTAZiWYuB94j0z6jnNQWbxzttIcsWwuByOPTBrSWJurnMsmjzWkjz%2B6%2BCmi3F5Jd3uuyxqT%2B6VRtwOc55%2BbqPp39uO8U/DXWGLmw1AXCoB%2B72BTj6k8/XNeseILuRS9pbWcob%2BKZ0PTPQZ789axrnUY4rYRQ6mqSRkFvMUZOO2cZzXI8XWTdtjseQ5bUppSjqup5Novwu8a6xcPLb2728UDmOW6nOI4yOcZzz17VW8Z/DXWtFV7qSeGdEABMMTKTnjpivbvDdzqd08tzHaNK0a7WK/vCAcjbg9O46%2Btatkt7JcGK50VJ0bh0nkbdGOOgBGcfjU/X6kU1LYzXB2W1Ep09JLROx8myWs8Rw8RH1FNAcHGDj6V9f8AiD4HeCtctluL3Rhb7hlGibnJXj74B69ckY9a43VP2U7eOxXVrfUo5SJdrWsEBbCnuWXIU%2BxzmtYY6k1qefiuC8yg%2BaKUkeB6XaXswb7PbM5A7An36AUl1A5jCLCQytyCOle12vwJW4Tzok/s1REW3XUxG4f3gNhP4da5PXPhrFaLKLrUInWP7rREgtz3BH8vUV6tHGYepTUE9T4jMOF83weIliZpWvfrf/g/geauj52spGPWpbSQQlZIywYHn2q7dWUcf7py25QcALgA1UlsmDblz06EVUORS5kzzK8a8oezmrel7f52Ou0TxZqGpxfYZNXSxiC4efzPnPYDAAr0D4cfFTx/4T1Zl1fxJDqdsV/1H2nleMjB4AFeG%2BUycncCKtWepXlqTsmcE8EAmvShiqdRr2qv8z52WAxeDblg58t/Lt1vufbHhD9or4a6hGsF9ef6WRmTyk3JGccKW4BwfoD%2BtdCvxc%2BGd9D5EmrWaK5I3Ncou3vkjP8AnFfCtjq/iSOVZ7SDfsA2r5QJI/LNXdPl8S6lq2LozxBuS5LJtwc5/CrVHCTaaizpfEGe4em4zqQcUuu/5n1t4zv/AAxd2zX2m6rbSpKD5YgmDDg4Jzk55B/KvIvFj%2BHI5zJPBOGZ8hpGwWJ9B1P4VwH/AAkml6Db/ZpNcYNGWWMKgLeuc5HGTWBqPibxB4y1CHRpbwzJLMqxbYgDnoDgdTXoQq0MLG0VeT6HyuO%2BvZ5WU6loQjq3Z/er/kdl4i0W31a2EOlGaRztLH7HwFPP3tuTx2J/CvQ/g14X8S2mnW9mnhPUZIooNq3H2LajY46suOe2OtcN4N8JfFeK4Hh/QLm3nkjONjIQ4TGRnOM464zXpF/4U%2BPPgWxTWbiznuGjXCxQyKOfz4%2BvSu%2Bg0p%2B0s0/68z5rGwlOiqKanTTvdP8AN2svmz0XQfAHizWd0VpoLIilWLTtGuME8YAOP0rF8bfCDWdIzqmqtsKHcTFAHTAx/Hj1OOOKzPBnxH%2BMunPHe3Gm3DM8fEcynaMdBlgent681g/GL4//ABc1qxOlyWSKuQWO/apx1yucNXb7acY809Y%2BjueQ8JgasuSgpRq93KPL/XoZXjJtCj1dbay1lLgrFvm8mIEIfTgc8d8fjXD%2BItc06YmJ79olAOXZX5znoAwz1qpHq41G6e41GC1tM/MyRKUzgdARWZPcWctz520tETlerH9T0rzq1f2l2la572By76u1GTbsvIfH4dj1W6F7JqMDRs2dqyAtjPQgksOnbNdPMnhCytUgttX%2BdePlRiB%2BYya57w9aaxr1yLOC4MEGAJNgJ46dhgfpmte%2B8LaDZ2ZWW/mMy/3Tw49eMj9azpQtFuMV8zpxck6sYTm9NlH9dBZNX057TbEryN/CAMD8c1lalJHMp/dhB6N1q5Y2tk7IdO0u5UtwWcZVu2aNT0Zo3IZCvH3XXH4V81nGDlF88Y6M/WeB87p1/wDZ6tT31sn2%2BWhy8qbpCRG2P9//AOvRWjJpMbOW2nn0J/wor5jkfY/VVOPc%2B4F8C6BcaxHHbRNFK6tMquoMY%2BdjhVAO0KSwGe1cl4s8OsttKLqaYXHQi3iVyygnAywBUE4xyfp2r0ZdZ1pCuk2%2BlvHqKMZZXt7TzVAONpRjyT83Ppk8d64rxHovinRNekbXVhiM7lw8uO4/iU8Z/H8sVy0oyU9z9Kx0qLw%2BkXfroeU%2BIoP7JXNxZwIZxxIsoAGCByo5H49a56wtNZ1m9MdmrN8uN4PBOegJ6V2vjbT768nWO4tP3EcmfMiX5V56YwMZx0p/hvw/Dp9ubspIoY7trqFIwecEE5FerFJRuz4GtCU69lsYFt4V1bTZWnmmktJQu44jLb19c9KW18V2mmyxvcXM8xhBDFvryT9c16ZZ%2BCbbxHZrcC8YO4w0ecAg9Ov5%2B9QXHwi8OWodrl3jLMS0cgGPbPHTr%2BfTOMc05QT1PUo4PFNL2exy9l4rttTU2ttbDcQcLvGAOOOOav6frc8ciQwaWrnOJHUAADvn9e1XLn4F%2BIUX7bp2hTyoxzBLY7tnBABycHb/ADqD%2Bw9Y8PH7B4i05kMz5ZjhwDgYrF4iP2Wd0MuxMf4kbFXV7q21jfZtDHlhuYJgkjrzn2/OsgeF9Ikm8i10EXEyYaWRm2BR%2Buf0rqrHw3e293LI10EjJLSOy/JEvqM/ljjOTSw6UGuz5epSsrKVLQxEg89OBjt/nFc86s5andRwFJaNB4F0VT59/cTSWljbjcpeNmVmzkD35Ge4GDmpWa8lvpF0pjOQfnYuFX5eMFVAPbv61u6NpLKqPcn9xvyyXOE3e3XBJ6ev612ui%2BBtPmkfU7y9liaVP3dpBbhgw4xgEHPXqCe9cU68Yu8mfS4bLKlWnamjzPVvDEcmJ01hXWNPNaFS4G3HXk84P6irVgl5pGkTQafd3CQkPHPcKwOG6EMnY89SBxjj17rxT4ettM0pobXwy00bLvW6MI6dCQB1PbtU/gzwwvjTTJIrGwjt4shZ5biHAY9cAADdjgjn8exTr860GsDHDS/eOzPNruXRhoC2ZezlLj95JMgLDAOQWCjJ5wRnHPtXknxH8KabrIjfRNRt4wpbzFW3yw6HgZ5B7fjX1N8S/grrdn4TnvbG8iuYYIiZdkBRoUGDuVckYGCSc8da%2BYvGWi6nCHjtZnufT5W5HqP5nPrXdgprm3PlOJMPelrG6Z55afDO8k1MuIY5kLYWR3UA/wDAQciuo0j4M2WsXBs5rPBbhTDyuQPXoOn51UsZNYtLho7SHY33ZNwOD26Yzn/OK9F8G6b4pMCy2TQsQBzHGBsJ4xkjg/4/hXrTr1ILRnweEynBV6nvw2OQ/wCGeNNKyb9KmTZwruSBkdTnofwrmJvhRpltqToyGQtI21Ps7A8HpnGCfxr3ePVNYDP/AG8lzhB84llAQn0OBWV4l146iRHpHhmykKEr%2B/fccdMBsAsO/OcEnHGKVLF11JM6Mfw7lFSna1jwLW/BeuaNKbrS9LuUXdtG8bc49ADmuW1XXtfdhHc3jAqcfL1BGO/Xt619G6/oXii7R4bwWdtIF2PNk7GUgYGCMdCOK8a8cfDa/wBNvTGlzaSlictby5APPBBAPb0x717OHxlWUbXaPzLO%2BG8NhqqnThfzaOEnubm7fzbmYyNj7zHmrWjTJbXsdwk8kUiOCsiHof6Gug0/4U%2BIrydIWgA3A7S3A7H%2BtdW/7PF3pjJJPPOykAh1hwpPoDnn8PWulVYpqTZ4H9lYyvFwhB2t0Wh6L%2BzZc%2BKtQ1z7fYePdNhIYKyanlJJQQeSchSQCcY5wBwTXqPx0%2BOmo/DtI9LsdftLiRUj%2B0wwBZCBySuRtGcg9D36jrXy7rukt4P%2B0Qwaq4mjG0Mv3xyRwT0HFchqev67q0QtL2%2B8yMNuG9VBP1IGa9hY2jJJ2uz5KWW4/C81Dm5Fe7ezt5JaHtfif9tXxBqMDWiWNu7OcvKkRVt2ck5Dk5HTr3Oc15p4l8XeIfGdyJLOBYI2AUkEBjnuQST%2BVZGmR6FDEGvLbMgHzHPGfbHWpn1meGTOnMIwPusiAH6ZFX7erONpOy7I41hKEK3PCLc11k2x954A1%2Bwtke6kU%2Ba5Kjd%2BOearLo5t5BHeAdecZIz9R3pyXupSxiBZpNuclFPDH3Favhbwtc%2BI5WjMrReWpJcxbgfzI9amMISnaCdzSdapRpuVaasuysQWet3WnAwW8ziEjDhs9PbFa8NxJLbiK8ZY0K/6lnww%2BgHStO2%2BGV7pt1m61Lyk2E7/ALMysf8APoM/Wtmx8BJqwW3sfD9xOi8ieJQo6d8np/k13U6FZR948WvjsFKS5NfNf8G2pV8J63punwvCJ/M%2BQhd4xz2/L9aluobvWh58jhkJwCeD/Lmut0j4F65q1g2o2%2BkrawxjO%2BRly3HYA8/h7etHib4Z3Og6M93c3uE8wRYadR1HQ9%2Bn4cVrWp06lDkqtcvqYYCtjaGYqvgoS5/8Lf6W1PNbnwoYrh4/tgbB6gUV0aaF4YVArasgIGDuZM/zor4iWCw/M7SX3n77RznH%2ByjzQd7K%2BnU%2BlrH43fYdDS6i8nzGJ2y3iDchzgAo2OcD8uawk8aX3jfxNjTb1JrlkRTPK%2BPLO0bgqtk9c8dPTisvRfEHwY8W3EmpeMLf7Ayv5UoVvMlcIANyscJ0IA5x8v50rnXPCOiXU2p%2BDbeZod/%2BjGW4AdFXo24jIz7HHbkV5UMPTc9I2Z%2Bt183xKornqpwXnr9x0PiXw7qNvLDJeMHkDlW2vyAB6ex71DZ6RcRBUhgJZE%2BYzRZyT14PWuEt/jHq11crDLBCZBIWEgkc5HXHXn6iteH4t2Elu8lxbbplwFiYtj0zzn%2BeauVGokcdPM8HOV0z0CweG1tlS2hjWU8Ou0AE5wcCtK08K2mqXAku7iUpIoxChbhuOOv16eoryyx%2BKd5caq0csrpHgnCw4XJ9MEfn3r0DwPq/iq40ptSfUYliIAMkgDY%2BYYO3p1AH4%2BteViqc4xep9hk2LoV6iVrntGjeEJ7vSBoOlWcGlwuqtOVtvLO1c4DtkFuTnJzj1xWXrX7N2oWiRanYX3mRGM7hjfsA5yTwCOewzUen%2BMtei0VNWvmjadoBFAXUZHuExgAAnkAd/Wul%2BH/xBs7JY9J1yeWOWcZidUzFsA6HPJxk85714E5VoTuj9Ko08BiaShNHh/jXw5e6RZz2d3fiS3Q7CGhKkNzjggeuM5/OqPwt%2BGnxCuI/7Q0C3tPsWSqeePMY9/UlevTrX0R8Qfh7d%2BMbmTV0jglgMYCRKDubGfvAcY6E/Tmr3wKsdNtLs22paA5ktT5dtEYwi7gOZNucHoBnB4ParqY9xpWtqcdHhuFTG8zl7vkcV4S%2BEGp3V6bnWd7ZwxiWE4BBBLIpwOPm/XjjNa3i60sNE0oG4v1hj8o/Z45Lc7F6A8ndyAOwHX359Y8R6jq93fnRtD0WOFZ/laZY9pjxyc4OTkdO3t6xWvgyHxBbGx8R6as0sDI6ySkNkr6Z5PbrXjyxbnK8kfZU8qp0qajTPnTxxqXjJrGG/l0t7y2ltnW0uoihRv8AZIABT6EA9a7X4eaVHaeHbOFowjtCHkGwjDkZPGO3T8K9w8W%2BC/D1n4EN1qUSqyQ7RGkhTfxhQQDg9ua8607TIIY1SKILjgBRwK9HC1lODdrHyOdYN0K8YufNe%2Bnbp/mTQ2SfY5M2/nDy2zHkfPx05459%2BK%2BVPiJp%2Bm%2BKvD6XDanANgIiCKquoPBX3z%2BNfUvjm4j0zwbdxSTmJrqIwI6jJG8EEgeoGa%2BRfid4a0bw5bubC9YtG4BJkwO4PGOOhx26dq78InKo3c%2BZzmr7DDJWTXU4zQvhXNrN48dt5uzOS6TbQM8buQe9T674H8SeF5PsURl8sAHBJLY9eB6V0PgXxt4j0x5tP8KSR7pyokMm0hhjuFxnvx710ot9eeAap4kiYuUbYQvDAY%2BX5u/J4%2Bn1r1HKpB2Z8fCjg60OaF0/wPNdORpEeO/ilChTuEsgYDscA9Tj3OfSqreFrVbozJdTQhn3KkGfl7cDIAOSfzNev2vw10u%2B0xNTtYI0lcZSNZRJubBOMZHPsOP0qnZ%2BCbvSLs2k2m3E3muSwuoHAQcjBBI4BAOc%2BuM1rGvFMyq5dVnHU8kj8LNqSkR67cMoOChjyCRjuFyO3rWdq3wmmgP299Sjk24ddh3MMcY//X616l4t8JixbzoNSij2SmQMDkblznlsEgk9BxzntXNS2%2BtPdtKtmjKVJWRDwRg8kAnOSfwxz1BruoVXJaHzOY4GjTlaojltMhktroC3DRlF/eSCXaAOnJGf8muy8J6RrGtXi3d1byyqoxAsUbElcf392W6d6g0y%2Bt0uhbGzikD48zdEVKkY755xXdaL4%2BttFWKBJCgQABksgePcuM59Menetqjm9kcmBp4WMrOVkcj44%2BDmga4cz6TcRzupLAW4kC4zk7QOF/EkenTPP2f7Kngu9hinW7cy9J4ltmXa3GRg9h9K9oHjHSNRun%2B1a1La5ZiXeNUZs5BwOxH4dK1rW08L6lGZk8SzTxyJj7PcIrMd3Y7CDjHtUQxmIpaO6Omvw5kmYycnGMmz5O8cfAGw0m9ddJ1yJ5DJgRLAQu3nkMe2Mfn7ZrjNU8Fz6LfNZyoZSDtzEwPP4V9ZeP8AwB4b0a3mtbW8IuGPyJOm7rnB3bM9PcD3ridN%2BE2r32u7jpUbwp992DBeRwclcZz7c17GFzPS89T87zvgZe1thdPR3PD9L8OW2nyJeamfLUYKJJKCST7DkDvxzXbeE/GGk6RI0lnpEKssRba4Yq57Dp/MivTNf%2BFM9yk1xpngC4WJbcyTFrVcAgHPXJz2H4dK8lk8G%2BNbld9joaR24crHNPcoobk/d3H26da%2BkwONpTV6Z%2BR8R8MZhgp8uJTa7q6S9V/w5talret%2BJna9fSIoQ5BDQuGwB7FgP1rb8Dala6ZMX1vVYooWTncQfyGf04696zfBPhqXT7eWXXte02AKD5SPcr%2BfzFQR7g98V2unweAr%2BUabPcPfXCxk7rNQqFAP4dpIOB1xnofoO6pX5lZxbueBgsudOSqqrCFnpfr%2BNy/N4/8ADMV5FHpOpxzoq52uvB9cKDg/WsbxB4X8V/EZBI2hTqiuzBnBwoyT6Zrs/B/wy8ILIJrmxljTCkI7gMA2SMYI65B/Hmu705dHtog1i0KuXMZWeUPkg4%2BYYbrjPHrXnVaEKkHHlfzPt8DjcTh6qqTqxX%2BFf5nzRN8GL%2BKVonSLKnB8xgrfiKK%2BsY5tIMa%2BZA7NjDGKwfbn24orx3lcL/CfaR4nfKv3n4I%2BYr74b3NnfReW5eCGPc5RSxRiTknPToKTW9JlnRbCCydhsIWUSYB/754/DAHSu6ijttWhFncKzwuAAFJBwOpY/wDfP5VANNsNMka1tpECk8fxAc9MdzxXhxqyjofrNXL6Vd8yZw2i%2BC7Yzpapp8hIH7%2BUxscEevYf5611Fv8ADrRbIq7WkkpXLYIPoRjJxXU6I%2BmRXEUElkYpGPy7zxnrzzkH6471rapbWtrpzOzKA4w8gPCjGCAeeen51hUxLeh6GFyalTjzLU4htE0rTrcK%2BnpGGYDckQJJJAHOf64roNBiN3pKRz3Y%2ByvIB5YXGRkDBGfUd6zdK%2BFmr69c/wBoXUzmAvhFdiMZPUAgc4OeP5V7B4K%2BDng/QnglXxHb3TbiJUS5SQJ1AyQevI//AFc152Kr0UtWfS5RgMZOd4xsjkyZJmS2iULbgKjIRn8ufXnGDXfeF9Qt9Cv47%2B90h2j8vYvlsOVI9T0HqKln8PeCtO1Yq1/50aYx8gAkJHQc5xn2rUHh7StWsol1O7g06AfdYct04zgHk8cZ4A/CvHqOE%2Bh9xhoV8Or3u0XNZ8dWGqxJqMWoPbpHE2LSOTYuMfdPr1xwRVv4dfFHwjpss15qaw28wbIKzkyMDjooPNef%2BNNHLuuk6JqklwkgAVozgIpOOTwM9/oe1QaB8HdPnAvNb1V0jkbHRZDz6DO7nsB6E1EsPh3H3maU81zX21qUdj3nTfip4e112vrIySC1LGRmgwxHYAN1PHNUx8Wl1q4h0vSrULPdviMyMqMoHt/9f0PsPL7Dwf4kgtpPD3g%2B0nS1kbE0rzFNuD15JIyBjHp14NdXYfBrxdJDFfXGpz7rRFMLJhN7Y9eu0c9ufeuCWGw6ejPahm%2BZvWUTrvEmrazeWcGl380YGd0kUcpfBHA5P4/5FRafaxKo3EAD2rH0HSby1j2Xs3mzFyZHz1OfoO2O1al9cRabZvNK%2B1UQs5PYAZNaxgqceWJ5lXETxVZ1ah5V%2B0Z40tbXVoNNGoKEsoTJLF1%2BZsYJwf7v8zXzv4yt9c8c6oX0vSZWtYgG2ghAWHOOeCP16V7mvwh8GfGPULrV/EfjGYXN1dyAWiTkAZx90DqAOP8AgNZOqfs%2BaF4OW/0O5unjgjRjBJLISWIXOAPTjGTjrzXqYatQopR6%2Bh8jmuAzHHyc9FD1PI/D9vrugqNM0vQ1heZy21wSQemcH6eh/nj1vwZ8NNZmsxquo6jHfMbhmurN1d0UdVfaDjAywPTHfORjA8HWnh7wrry21tpdvfeWys0s0aso4PzqxIwACRnPUdDzXokHxC1ubxF9v0TTIpE2qkUcK/u1JG3KleQOc5OTnH1rSvUnJaI5sqw1Cj7s5X8hPFum6daWq3vh7w7diOAKXSGImOc4IypwBtz/AJOana0utQa01a4uVjtoxumjvZhudSMgquRyPXPHX1q74n1LxtpsbQQ2KJA5y8ReONlOT8w%2BXLHnk8VmWPg6/u7ZTq/iBBaRj5jcmM/Zyc55YkYIUHryQetZU7t%2B8eliHGHwL9Dy/wCLd9Z6Ip0rRnV/tkrM0obeqIDnC5Jwckc4BHrnp5%2BEbk7m%2BYYJDEHBr0b4qeC/DMF8w8N3SqikyPczs0ceD2wwwDkdehyOeK8%2BMJRyrYyDzg5FevShaKsfBY6sp1pKVvQ5i90LU9N1Nbm0mBt3BVRnB55IPr0rr/CmmTalYgahdyGJVyyo4UICSATg5x8p/KmxIpILxq2DkblzTtSm8S6lHFpUd%2BEgJPCjk9sfX/CvThPmVmfHV8KsPUc4XafTsYl7PpcF%2B1rpzO7GUBJN5bzSODwOep6nB68c10PhzVbyETadIbuaN/8AU/YojGEb33AEgdPXr61FpvgrUbXXY2kkgKq%2B15JWRuSRjoccnjt2r0zw/HFoemNdHThHGhP2iZ5VHb7xYD/ORzTnKKXcrB0a86l2%2BUzNO8L6VcRJqF9pV26MNzJPO0cQbpxt%2B83t74NdW2r%2BHbe3i0%2BfVbOzl27kS0mySRkbSM4H44rKg8FafrVtNLeWwljmPEjOxI7Y25PT1wfpiuy8AfBm1022LWGh3Dhl3/KgYydtzHA3Y6ZOcc81yVJQW7Pp8JDEvSCXqcNrOi61421VZ9EvrT%2BzkC74oIh9olbvkkdOh44Ga4vx54Q03w41xcWoe8jdmNx5CqyA9tpKHbz1B9RX0D4h0LUbqCex0eyuUlYEyEtvO/HJw78npz0rlL74a3Wv6F/ZetyRKQoSNLm0UFUHRRt7Dmu7BYiMJJpnz/EOUSxVGUHG7fVnyadD1%2BWRoINNZ9yAlZISoB9AScfoK6T4beDvFviW4EvmLp9sMxqXDZcKcEAgYIyMcGvoTRf2d/CGi2ckkF5PNc43IxjxFG2fQbSQPTOcgH2q3a%2BHdN0mSC3mjMsSDG3gL167e38zknua%2BkjnEuRKJ%2BMT8OaUcQ51pXT6Hmln8ANU1WVZ7bVdSLscPcb84wPlPzds%2B1d34H%2BFFz4VkS/GsTMygeY8MJYsQTncenUk9BzzxXpHhG306ODyYdLdl7Mqk4HsM4/StjU/C1peW5S/8PO0DJgFpmQMMAYIU/L09KuGZ1amknoRX4MwOF/eUY6rzZ5zc61rNnO9qdTjYocE/Ic/lRXTr8PPBsQKR2OmxgE/I1yGK89CWUn86K7FXodmeK8vzXm0nH72fOf9mS6fKPNbYiwqBIy/ebJ49v8A61KlnIjIyxqCSGAUc4555zz%2Blehaj4MmaVbjTkE6HGSFA2%2B2O/8A9etLSvCMbIsdzalHPJwByPTA6/pXws8RFRuf1LQyypOpa55taabfFJJ4r9nkYhUMkgIx9OlTr4R8a%2BIbmGyubl0gBLMwwcc5A9O2c88H15r1pfBOkQxCaSGaMMOCka7iPatzw/Y%2BHNLJM2m3VwySbtkoCqw6Y9/WvLr41xWiPqcDkCnJc82kcdoXgi8svDsVpBe5WJTgykHHrxitTQvAfijUbiOyju4o4XO5pQSCQpz688jHFd1F4p8ESbdNFp9kKlWaNoBk5x0J68e1aul67oEFh9lgkUtyWk8zenU9QmSOO3H4ivFrYmo76H2%2BEyzCwSXPsYyeGf7I09bW8gtbgxnLLCpZ3OCAM84xwauXnhCR9NWS3sYbZkyAycuV47ZGTT9a%2BJ9npDG0k063m/vlbV0G7/gWM1hP8UvGOsOtnDYiJSSfliA4Pp%2BXvXNFVpHo1qmCpR5b3Jz4WFtPJdmRHkZ%2BXkYFm7HOfqfzrV0bWtQsJ1SHR7aVYhhhKDgHpkdhTrPw/qusQwrMwYSEF2D88%2B2Tj6DFdBpfg2HRgbi6ikKbQHZkYgKPr0onyrSTMaXM9aasixpet332ZriXSrWNiAfOBzt9%2BM844AJ9K0x4zuI7NlfY8jRFI3TPy5GB7Z5zUFlY6NI8UySTSKxwsQ%2B6c8Zz1GOvFTeJNPtbSWDT7ZAo2eY5BznPTv8AX86y5YN6HROpWjDcyrZI4ozIQBgVzfjfVlSza2CwuZ8oVnbC7ccngg%2BnQ966HU54YY/KUjgVG%2BjQHw42p3FzYsXHmfZ549zYDYH0Hf8AGtqduZNnmYi6pOMXY8N8e3Xh%2Byt2C6nZwzAEIHdAEwp2tnOSM49R6V5Dq0Hi7xO8llLrfmWbDcEhZQrEf3SAST7596%2BgV%2BEt1461O4e7trAxDndbugILY5C52%2BnTJrO134OX%2Bk3YsjqVyYUAAEb8EE/KCF6dRXt0K2HjpL8j8/zPAZpiW3B2j6nz1YG/0pntbdJYzJKBKxB3NgEYB5xwM/zPNX08WeJNIvmEPiWWCCYIvyc7SM/KNuO3X%2Bteo618NNKgRNJa2KkoS0hiBPXpk5x9a5G78LT%2BDYpLKWIx4uN1rKyZfIYdAuc8joSff0ruTpVVZI%2BbdLG4KouZu3dXNkeK/Fkmmfariw1S/clEhM10/HodmfcnOOOPWq1/rnxJewBm8O3UxkHDQyhl2kYAcEHc3Hp6Y610%2Bla7r%2BnaaDr2pxQPcHIeE%2BW4XjkFAVJzgevPaqmi/H3X7DULrRPDNlZzxv8ALHqE0UQdFIwQDtPfJyMdueBWNOnNSuoI78TicPKklKtJM5bQ9P1bX9ae78ZabILSFVSRI2KouMY3ocHI57kZql4w8HyNfrJoFmsVvGHZ/O/d7k6gqMZJx/WvQPEmrJPpckN6XLugLCB2VWzkkk4yfz7fWuZ0HU7Ga5XSxE3kBsKhRiqHr2OffPvXowjOS1R8xWnQp1FFTu%2B5xMdo64DAj39a0dMhaGVZ0OGQ5BNel3fwz8R/EeQQeG7OOaSNsQO7KGfgnb6gE8ANxkDB5rhLzQ9T0bUJdJ1azkt7i2lMdxBIuGRh1BFTCUZOyOitSq0oqbWj2Z6n4a8RaV478Nw2Fr4UVL60Hl3RgVT5oc8Y3N0yuMc45HcZnPw716xhbT9atZPNAEhinXyQFx168j6VwXgfxPrXgfxBb%2BI/Dl4Ybq3bKNtyGHdSOhB9K9asbzT/AIn2UerWWnKb6KXFxYmaNBCG6sm4fNk%2BgHXnJ68Na9Gpo9GfSZfKlj8OnUXvr8fP/P7ybw62gWOn/Z7jRIcrIFWdSG2jcOgHPTvW813JcsL7Tdc%2ByK3zKAQgHbOSQW7VoaR4M0fRtBkuPEE8X7liZIoF/dgYyQWwMn8hVMePfASXo0az0UyXGMhBluOnB6dO3FZRSqS7no1Z/V6aTsivf6W%2Bo/6RJrFs8jD7yIzk%2Bgz/AEpum%2BHreBSttaRk85eW33ZP0Pete08T%2BCF1QQ2cC/aiuPIjhZ2x77eSfpXY6VNbXkSTwwQJuHyIykN%2BOeldMansjyalH629NTz%2B6%2BH2v6nAyRSLGjphDBCqE5Oc/pTNI%2BBUCxLdai0zz5%2Bcj5icHr6e1e2aHoAlC3F1bqu7%2BELnH8qs6noem30B055wgPyttYqfpwcitFjlexzTyG8W0tTxW6udA8J7rZdzyw/eypyCPXArkNV%2BK/hq5eW3l8RWschlIe3N6okBz3TGc59SK7n42fBa11S2utV0%2BzvZBFCBkyzPggjJTH3OoyQRnHNfNnjH4ZeOvDuj3WsLqF9DCkOSsbELGhBYghiMDg9D%2Bor67JlgMSvenZn4Z4g1eJ8pu6OH5oK7b7Ludhf/ABBjgvJIo9FmlUNxIsCOGHru3c0V4DPd60szKl/PKM8SLKwDfnRX16y/D22PwGXFuauTfMv6%2BR9DaXLYXcyTCULhBuBbGOvvk106rocGnxyPqEbDq8bAEj88YFeU6adYsiqaZOsymEFSCPk68H5v0NWLuD4i36%2BZb34jVgAFjkGTj2wc1%2BN1Iwa3P9DsJWrwlZx1O71Tx74c0WRfts1qijoq4MhI77QayPEvxS0u9Qy6dfmUH7sCx7XJz06/5FYWg/BnUNdkR/E8xG45ffxz7cgGt5/hn4H8Hzm4u7ppERcuvkFBk8DHXJ9AM/hjNefUdKJ9NS%2Bv1I7JIwdN8TS3DusKhTuPG1yR7HA/rWkniPVTEI7W0IkbkBVHOCME5z6ehrqNC8b%2BAdLgATwQszIcxvcOp3eh2AAZ/Gt6%2B%2BInhnV9NW60Pwra6VMeG1F7NXGM4YKgBycHB44znsM8U6qvoj1sPhXb3qhxvh9tdvgIdVtmmjY4ZFuX/eexOPfoOhA9K7/wvaQxsiSaTaRyogPlAMOP7xJbOf8AIp%2Bl%2BFvDesER2%2Buh5WX5wgYAj65/r%2BFaLfDuK2D29vYzPG7gmRnIJx2IzWLqqSsztjg5Qd07nR6L4s8IaVA1zLOk91Gu5BGc7Wx09/51taRqY8SuElmEds45XZtxyOP5cZrlbXwTZIixjTmiIf5McD%2Bf0rZ0fSL20cxtvCNwP8cVzSUWzvhOrGNjqms9J0uPMUsR2jiQDJQd%2B/p6VxGsak1zdz6jNJuaVuCRg46D9MVvazALPR2Z2cZGBv4zk%2Bn51xOr3w5C5wO1VGJyVql9yhqV088whjyzO2ABzXQQeG9S1a0kitPDkDAx7BcXAIO3GOMeg9cdKxvCAeTV21NrLzxCMIGJxuPfgjOBn869FgjW7tyXttxZtxKDIz9Ov5k10R0PMn%2B8uji9G8KQ%2BEJTEbtVlLjMTM5TGPp/Ouh17wdaeIbWI209v5u35XESlBnryOP0zVe/%2BE1rdXa6gHkXOdsUuTjnr/8Aq4571X1Lwn4osLY29pe3EMCDJ2ZUge2BkVsvZy6nHJYiC0jdFfVfhq9hp7ada6zpweVd0uYwxJB7jaMj8T7CvLPiL8Pr6%2B8q1kvI7xlOyOVY/wA1UMuT9fbJr1DSdN1O1zY2cVwzEbxPdzMWdgefvN2A681q/wDCK6xLp4uOHlJzu2bvl46Z4FaQn7OV0zixFCWIhaUT57tf2aY/EUrHxX4oWGMjJRLnBVsHj26kZGeuKw7/AOFVl4Xnji8MQTXnkgLEtrCrFmGdx3DPbPsc9K%2Bh4PgoniG5LXGpQROykhbdMNkn%2BI8inj9mez0e5F3FrFwZFi/ebsMpbPPtjGB%2BFdcMfyPWR4tfhuFeNo0bed9T5Vk%2BDfivXNSFz4g1O7jtIosyQK2yRjnOMHgn36%2BwrZ0v4X3%2Bn3Mekadp17PI5XyzIzMwbOVyV4HUdPT3FfVVl8JdC06HyLuB3RjuRniyxz0%2B6cEAeua6Lwx4E8N6c3ntbRu2c7pmy3T2/wD1Cm83n9kxp8D4SLvN6njvw9%2BAnjGz0kahcXkkMjYEkm/K7SSSpDKeM9QADxx1zVX4n/s8a/40fbp4glv7aPAvlXakg67GY4zjoMDI/Ovo19RgskMNvNG8UeAUC4yo746Cq0F42qO0FpZkDdkFz8oH/wBb%2Btcax1dT5j3XkOAeGdHofBj%2BHr7RdRfTdWsJILmFtssMi4IP%2Be/cGtrQ7i80i9iv9OcpLGcg4yD7EHgj2PWvoX9oH4InxbajxDpBH9oWwck7l2TLnOzcTnP93Jxy2eua8Hg06e0ma2ubdklRirowwVPfNPFYxyhzHPlmTqjX5Fqj0Lw5YS/Eu3L2OpGPUBjz7e6kBiCgcsMjgduBxwOOtdZ4Z%2BFOo2ssiNdQmXZ/rooQM5zkqzc8DHTHsfTy/wAPy3%2BkXkeoaddNBNE2Y5Izgg17D4W%2BK02tW8cNzptzcXaDDwRKHDjGCR/EB3xg45z61zYXNfbT9lJ2fTz/AOCezmXDP1bD/Wowcodf7v8AwPMoDQ/DvhaYxafE19eElUlaJh8hJGAP4hxnvnPXtXSeGPDdpbAa9qLTfuwcKyFUXIzwoBzWjp/gr%2B05DrWp6e8YkPy227bx2JyB/ntXT6P4YWYbGCkYxuVQuD6Z7gdq9OVVJWbPCo4SUneK0Mldde/hZLLT/wB0kgMMUs2wy8/3WHTvz1FaelvqW4XX2eOLc%2BNuzLj3yPp2A4rpdM8NW9tveSFHIH3ZBjI%2BpH86uLoEFwq28JjVz9yJeij8Biud14dEdscLUteTOcvNQDxMl1bK27gLjG/68YFcz4107wld%2BFri21LT9PKKu0RyqpVscYfcCCceoNerajo%2Bn6HpJuPEV9bpthLAjliuOcCvnH42%2BLtK1uCXTfDeliMLI0YuULZcf3jgfj%2BNetlNGeJrpRul3PjOMMww%2BV5dOVW0m07J63PKPEXw2%2BBNxrU81xoQWRmHmCLJXdgZx8w4z7CiuW1Hwrrt1fy3CXt1tdyV2ocY7dxRX6lTw9oL9%2B9u7P48xObQniZyWXQd2/sruY2mRPaXStBcFo0jAc27krkEnBxgd66rwx4r07zUjliR2SQKDcpxnqOhPr3rpte/ZqttX1CWXR/EEtrZSFdqwHco%2BUA555ORWVrX7N%2BraHbgeGZ2u5kHz/aZFQkj0JyOnrjFfh0cfzxsz/RqfDlSjUcoLQ7rw14mtfEFg1jZ3K2pCg4aLzSwPXhgcDv079qsab8GNCn1f%2B3df1MXaQt8iLH5YzgjOegHPbFeP3vjq9%2BGKquu%2BG7y3njHLq6uuR65IHryOnbiue8WfttvpVvDDaajNKy3A%2BVokXyxg5GeS3br07EUKnOt8LFPGYbAq1fc%2Bk9W%2BDXgW7sZmMcaM6/6uGT5m44yxPp6e1Znh/wR4T0C8jYeGESTOEmkmMp5BHO77vfj/GvlrW/2p9Z8YgS3PxJvLKB8b7IOhTAIIyQWPBA6AHNbuj/FnTPEun22nT%2BM7rXZDIqRadC7AR8EBjuwM4JHHPzUnhakdWzOnnmBrytCOp9a6VfeGreVZrW2s4y7YLSNtzk9%2BBnPtW82oW1sgmuprbHQeTJux%2BQr5usvt2j6bFdXEi20ToFRb%2BZZtq44AEeSDnA5A/rXovwxsNRk0qCW68fWK22S5WBypUZ5Bw2wflmueUVE9OlU9s%2Bx29/460IXy2VjdyzyMHPlW9ozsxUEkA42g8cZIH0FXU8T2c6mK30O/V8A%2BZcIqgevK5I/Ksa/%2BKHw08NWkq6Lq51TUYwFjsLdDJvbt9xTnjnjsKm%2BHHj7xB4ntJ77xF4OurCTzMQAwMquPbeAw6dwByKw53FnYsOqkbIh8U6tbsAsUkhbkyBz90%2Bg4HFcTrWqxrnL/rWl4m1GdJpTc5WTzDvyOhzzXC69q2S3zH866oRu7ngYioo6Ha%2BDvit4G8I6cIdWsb6WdpGeRowuzdnjv6Y7V00P7UXwmRdk95Nbk9XltXYL/wB8A188atqygt82fqK5XWdYXDDd/wCO10qip7nk1MfKltY%2BqZ/2n/h5Le%2BXp/i6x25/1t3IYh%2BAZRj8a2tN%2BOvw2v4xHF8StDeXBYodWiYjPbGRz7V8Ha1q4O5fMNczeX5nl74B6k1p9QjPqzmlxJUpKzgn/XzP0VvfF19r%2B3/hFxp%2BobeBIZQwC9cbUIOOvf3qbw6fiNeSE61BbusjYWCBdgjA4XqCck5JBz2xjoPzlhupEIKHBHQ11Hg74yfFLwJE9t4S8c6jZRSSGSSGK4JjZyACxU5UnCgZxngVf9mzt7sjn/1soc37ylZeT/SyP0m8OeD9Wu4pUku9uTyRIq7Pbgk4retPCUdu6xzarEwGNzyRlieufTj9a/Nyy/aU%2BP6kkfFXWvm%2B9m8bn8q9M%2BHf7cXxQ8LaDDo2qaPBq5hzsnubp1PJJyQOvX8e/NRLKcSleLTNYcYZZKbUoSiu%2Bj/K59w3XhjRmcxre/OV3CNIcYHqBx781jax4Ls49On1uTxObO0UHdcsVCLjtluM9utfMFr%2B3LrGrQtb614fawErhpbjSCVmPsGLjH4D8a3vBvx%2B/Zihvhq2paDr8d6ZN8lzc2qPvYnOSVlJJ98Vi8vxkN4nZDiDJq%2Biqq/np%2Bh6/Hqfgu0sp9Tt/HULpBGHdZpdjFc4Jw2DndjjGevbNZ/h74naHr%2BpNYaZZ3EiRtgTiIlXOcdQCP5VD4a%2BJf7OvjyYz2PiHS7c22AU1JBas27%2B75uA/wB3nGcceorsbHVPB9wnl%2BHdTsJVP3TFJG2PptNZ/A7TRu5usuai1byIbnTLAlpDG5MjDKvOQCfcZ/QVxfxZ%2BBNj4ygbWdGgW11WJAFwNqTgfwtx19G/A8dOz1HT9StZjIQZfMOUBXgdevHP0qZf7VhCAhY1Kg/OmAM%2B2P8ACqlGnUg10Jp1MRRqqfVHy2NFu9MvJNO1G2eKeJysscgIZSO1bWgvdaddx39hO0UsTbo5EPINex/FD4NXXjJf%2BEg0CMtqEUe10YgC4UdgMnDenQV5Q9o%2Bmh4rqExyRsVeN1wVYdQR2NfB5zGvg61nez2f9dT924NeCzjC3VrrSUX0/wA0%2Bh654C%2BIdv4rhFhemCC/iQMyE7VlHdhz%2BJA/lXd6I88ZLackUju2G8zIwcc49a%2BULnxJd6PqUOrWFwFntpRJExAIDA5HB4NeweD/ANpS08W6Tshit7bUoY8XVsXIz/tpgcr7Dkd%2BxP0OQ5lUzGKoVn7/AEf83/B/M/P%2BPeHcPw7OWOwkf3H2lvyP/wCRfTtt2Pao4o4Lcx3%2BqIjgfvEXJ/M9qzZ/GVrpDnyWQuOI9zgAe5/GvNp/idA75v8AUGYuB8gGQPw59%2BD%2BneO21bUfEkpa3tZoLXd8s3IMvHtj/Jr7SjlUlrM/FcXxVQqLkoatmn448W3OoxS3N3fIJOTtt3U5yew/z/WuMl8H6heRm4miOTkqHjA25xkA4zn9MGu4t4dJtVULYNM%2BCu0ngDOeSc96uNp%2BofflcE4wEXjFezh6kcKrQVj47M8HPNZOdWV/JHl3/CCWBJN3YSiQk7gs5UDn03UV6K/hxncsLe25P8Vzg/zorvWYvv8AifMvhanf4F9x5dp%2BpwSaf/aOl3Iv7FxlJbO4Vohg4O352zg5B7ggiq2u%2BPY9CjZReWdqmMxtLcjO31wcH8Ofxr4x1b4t%2BJxpdvZf8LDuXskhEcNgL9mEcW0YQjOF/Dj%2BdVfCXxk%2BwXSxNpk1xGpIz5r4/POK/O6GX8%2B8j%2BqMfxdGgrez16n0n8W/Cnw9%2BM0UVnr/AIkiDxEhFjuGwS2MkhBj8c1wB/YR%2BDkFxJq03jea7t2jZGR0crDLkHJCEuSBkdMc5z2rE0L443klyU0rw0Y5G%2BUSXLLxnvk8f1r0Xw38Z/tVzZw%2BOrhbezUESeWY4fMGQMcbd/cdeN3Xmu2WCnSXus%2BfhnuAzCpevTV/NHEaj%2BxN8J9PtW1KDxZHBEHXyzNOvz5POACzrjr8wH9Kb4X8F/Br4c3AbUdV1DU/KLKYIFARGJ4YvkED5eMkdfau18fftI/AbwyVXQ/AN7q07f8ALYwtKY%2BenlufbqCBz3rlJ/jLqHjHbceAvhxNfGQ5vYLnT0sowSTsXO4lgOfulMc53Dpjy4jZ3Oucslg%2Bam4p%2BSubWmeKvgnrMqtFoOrNKvKxEK6E9OjHAHIPHrXoPhHRdO1iOBLC1tEjnbEa3QZfTn5SMdvrXlVt8bviCb%2BK1X4Z%2BHNJMIMVzbxwB0uE6Nnhj26hs8VYk8Y6vc6y%2Bo%2BG9JtbK5YjdFbXEjj7oBKeZICh642gY4%2BtZzhJrY2oYmjGV%2Ba/ysfQV9B4N%2BFHh%2BbxofD0E93bo3k/Y5dxdumxWkPXGR/Q15dD%2B3D8WNW8QNpei/AzzLYAbQbvMgPc5C7ce2Pxpnhq68d%2BO7mLw/4ja0sFtV3vO8TySP8AMQN2wtljn7xwOBzzXoul%2BE9G0W3RZP7MmkdNr3NzZseDjnkgD8K45unD4tT2aMMXimvZXij5/wDjH%2B2N40vZfIi8Madp1%2Bs375o4yzyKM/LgsV7jkYPGPavOIv2vdbk3x694YuZZcABLS22Ae5Lk%2BnTHf2r7O0j9n34KwPc6/rWkWWpXF3tM7MqMAVz8yqcbM59TmtC9%2BDn7PWo23nzeA9Nm8oHYoXYen1zWX16lT0t%2BJVThrMcTLn9ol/27/wAMfBl/8fPivrx%2B0%2BHPhduticBpw5LH6rgfhVRviL8YLtmF78GbxjkDMLyDr/wA1%2Bg3hLwf8K9LAtLD4awQKR%2B7ZVBB/wCBbQf51L4u8L/EzUtThg%2BHLizhiB3ltNjcsDxjfgsR14pLN3F2SFLgSc1epVd/JJH51ar4m8WQHZrXw/1CzY4yrMCRnpwcVSs/FFrcPtk0y8iIPPmRL/RjX3D4w/ZR8U%2BIdVm1T4jeNI7drhg0jNbYBPbCrjjHsBWFrn7J/wAEfDFqh13xVLcyN8ytHZSBf%2B%2BlU8fpzXVTzqXkePieA5N%2B7J27/wDDHynZ3EM6hkV%2Bf%2BmZ/pWpbabchvmt34AJyh6da%2Bh7T4dfAXSWEdpLeXiqgISOMr16geYq5Pft%2BNbtt8OPhF4ggeTQtQ1CyvXQAWU5KtHgAcgI3Xrnd3/Cu2nnMlvE8SvwKpLStZ%2Bl/wDI%2Ba7aF4jkqv4mtKBmRclCCehHSveY/wBjzxn4ilM/hm40t45yAr3Fw2RnjOQf54rC8T/sN/F7w2lzfX%2BvaTbxQMxSOS6AMqAkblzyc8HGe4rrpZ5h2/fTX9fI8fF8A5pBL2E1L8P8zyqGa83bmj49wa07Ke4cqUi49Quaraz4T8baHdtZaVp41CSNir%2BWjHOD2I4/MVo%2BEvB/xz126FpafDeZ1J%2BV4ZuR9ciumeaYepC0GeVR4WzPDVv30Nu2v6Gnp144x58ByRwAta1tcXDjEVvgHvit7T/gF8YxEklz4MvFkKb2iKZKjJHJGcdOnX25qZfhx8RLCQQX3hK/jYno9sRXz%2BNrTltqff5Ll8INc%2BnroT%2BEPHXj7wpCLLw94kvLWJpDIYo5Pl3kAE46ZwB%2BVdrpXxu%2BK%2B/zZ/FDSkjnzbWJs/iVzXHw%2BGvENlcG2vdJnjdMbgYjxxn%2BVbGn2s6YWWIj2xivk8bj8RR2bj82j9byXIcDjLOUIz9Umegad8eviKIUhkurV1QDG63xj/vkjNcp8Q/Hdx4nuvt1/ZQJc4xJLApXzPTcM9feqd/ObaEp0z1Ga5bXL7O7L/rXiSx2MxceSpNuPZ6n3FPJMnyiSq4ekoVLbx036WWj%2BZR1zU3w3%2BNcrca/qGkXyanpt40M8TbkkQ8j/H6d6sa1d8k765HXL4nOG/WvQwlOUZKUdGj5vN8RTq05QqLmTTTT1TT6NdT6i/Z4%2BJHhP4rwtpN3%2B7123QNPZs%2BElUceYg7j1HUZ9OT7Vb2hjjEUUTFQAFXZwfpXxN%2BxXc%2BJ5v2h7NPD0MskQ0%2B6OpLEuR5PlnGe2PN8r8cV9pTS%2BJrVzF9jEhHBy4P/AOuv1TK8VicdhFKrLVO3rtqfy5xLlmW5JmkoYWnaDSlZa2u3p%2BH3GjbW0CSb5IZN6/e8vCgfVsVf/wCEistOU2zvDE4A%2BSDjisKFfE2qShJ7YpkjG0gKPXIrVXw1EkZ89Yt2Mklj%2BWe9ei6dNP339x88sTXnF%2BxhZd2inN4s0cStvV855yTRU76ZobNl7ePPfO6itFHD/wArORvMW/jifnv8T/2LL24TUNf8LXcaQiZ5VtJZTG8QZuEAI%2B6oOASRwK%2Bb9b8G%2BMtCupE/s%2BaWKNyBOoO0%2B4PSv0Q0Gbwtd6fDq2ian/aVtdwpLb3keoefFJE6hkZH3FXVlIIYEggg803VPDfg/WbdrLWREbZlx5EZCr0x0Hsa/F8LxDi8O0pa/M/uLN/DrIs39%2BleDet0rp38r7H5z2Wp%2BJYbhXE88RTnO9s10mi3%2BsXdzDfX5e4EQIPmBmUfyxX114m/Zf8AgnrjNLZxyW0jZYvFKRzzxznj/PtWV4Y/Zv8ABekR3FpdW8up5Y%2BSy7UCDjBJYn3Pr7evsw4thy6p3Php%2BDNb2jcKi5fmvzR5Z8P/ABVoUkYWbyXkQBV22MYIArrbr4qeE9NvI7JvG1npZMQcyz23nFRzxtiBHY8EgjHI6VZ1f9max197iDSrW%2B0oo22FzPGUfjqSFXHPoM1yuo/sUa3NYTvHYPePBOVMv2jaWGAQQWwMf4mu2nxPhKq1Z5uI8LM7wcvcSaNe7%2BNnwo1iEWmq/F1Y/KTYWsfDXkGTOOSd3OBnr%2BtTeGvij%2Bzd4TkTxBbfEG41e8WbL2E1kwzkcMScKcensPrXifxD/Zr8RaGpgsvDV6bkMPlRS4YexGc//WrnNJ%2BAvxTth9tuvB1wkKgndMSgGO/P5dK76WOwOIV%2Bf8T5TG5LxLl1Tl%2BqptdeVn3NoX7Xvw6uJIHs9Iu7guMLAFjjWIAA5G3O7JOMEjG3vnj1bwd450bxzpX9opocluCcKkwXH59%2B/p2r86/D%2BtXPg1lWSBGfowhQOEI%2BpGT1%2BnNeleCPjzdWLJc6Nc3SSRsA/lQIoJPYbj2z6UYihhqsLUt%2B50ZXm2Z4WrfGbdktj7ttNOsVPlypCBjlSMYyD3HQ9auaf4Z0J1%2B0G2AVeA4f5R%2BA6ivlHR/2vdRsdUhW%2Bku7iMoBKYinOCfXjPvivQvCX7XOnapOLe38PgBuXmugSykjuqscduK%2BfxGCr03zXufouX5/l2KXJZp%2Bh7tCtppcjCFDP5h6qwUL7DFaK6q9pB5ywzysf%2BWZkJz%2BPavL5vj34DWz8yz1JXu9oxHDAzjPfIBBA9KbpXxt1B5xc2fh77RGvTfAUbOeMfOR37g/SvOcqkHqfQqlSrR93X9TrPFFn4k8RCWX/hH7S03YxLJMHlcDpyRx%2BlYejfCvVtQuEiurdGQnDq3zkrnoccH/APVWvceL/GviO1A8PWWs2ssyr5rTXEaKg4ztI4Hft3rX8K%2BEtQspTe67rdxOzAZW5uhN%2BHzLz%2BAFTGvNy90dXDwjR/eJJ9Fdv/gkdx8FvB%2Bi26Xaqyu4OxIrchienTHP6etZ9j8CruOWWfTrlWR3O1pomyBno2eCR%2BNdq2s6bYvviMTMDx8mB%2BQpZPEsc6BxciHI4WJPz69K61XafxHjPCScV%2B7v5tGDD4C8R6NbiLQoIY2B%2BV/swJU4%2B8GZuOQO2K0LTWtUvLhT4j8N28LLEHdlKs0gx/dbtwevPvWnb%2BISsLTeaWVOeRk/rVLWfH1utn9quLKTaRtVo4/mIPBHOPX8q6KU7vc48Rh7L4EY90vhrV1kvLfQbQtn762ajH1IHWuUm16Ox1SNNNhdZI8jKOF%2BboM/5NTa94xjgvWhsodqzliZAjMBnueBjrzVYaXZaeFlmv5y8hyojtiARjrndn9K9Sh7NR1Z8vjo4iU2oLQ9D8I694glsw11rAiXI3yyhSBn1bHT2%2BtaN0b/AFuX7PGVuETBeeHaVI9RmuM8IaJe61I1tM8xt1CkxM3IBGRz0BPX7w/HivSdFSHS7cJpKyDH7tQ5JEbfjxj15OK39rFM82eDm43a1K9p4cg84vp1hCzja224IHb174qfULW9aBlaxt3RCAx25C%2BwzUxW6sdSWa6hhCTp88yzYBbgg8889eKW516x0yG4mhSSUuT5jK4YZ9QBXRCpzHk18M4O9zDvvAei3dubnWvC2lujdDPaoxP4YNcvrvwy%2BAiahbaFrul6FBf35dbK0a5WCS4ZYzIyouQWYICxC5IUZ6V1b%2BIoWhe6u7Z5mTneIgW9hjOQfyrldZ8WPqmqM9pcuFfhlLgc%2Bmc9P8K7KeDo1X70V9yPIxWbYzBq0Ks16Sa/UwtZ/ZW%2BB2pRFj4Rlgcjkw6hOP0LkfpXL6p%2BwV8HNYGYbzXrUnr5F/H/AOzxtXr3h5nWJH1GUQAH5ierenFXNU8Q6LayfZrA%2BY/Hzgkgfhmtll%2BDvZU18lY4JZ/nDjzSxE7ecm/zOe%2BDX7P3w3%2BBmky2vgjS8T3RBvby4PmTz4ztDOcYA5%2BVQF74yTXYSywIN85QA9SR/OqOkasNQD5kbIzgLswBnHrn8cGrl7FYrpzPZXgMo6qzg5/ED/CuynThTtFKyR4%2BIr1cQ3Ubu31YDV7CNdqTDj7q5HP41UvZJb58QTljnAA6L%2BNVJ9IvZriIRvHN5jYIhbhR15PPNa1tpUtsojnO05yAFK/r0NdadOmrrc8hxxFd2asjLXwzqkg3tqLqT28wUV0seirMglMwy3P32/xoo%2BtEf2ZHz%2B8%2BLJdUs9KhENzrNlYW8K/LFJOsSouODgkADj9KtWU6X9v9q0%2B/t7iLbu8yG4Vxt9eCeK4ubwVo%2BtX66l4iK6pMq433MeR77cN8o6cD0q7o3gfwbpMkkzaTbjeBuAj3YI7jd0PJ6V/MaxdJx3d/wP8ATR0K8Z2tHl8r3%2B6x1S3IhxJIhbIyNvOalTVmxzGVTHSsi3nsIYo49JjlWJRgKgIH5AVYBvpSCtszbhwCM1m8Z0RUqMftf5GsviW3iUKLaSQe4H9ac/i%2B6lwsW6KNRg%2BWduPyqnaaTfygmWApkcZOa0rfw6yxeY8G4nnmtI4qtLY46kcHTeurKR1aS5laa3aVp1%2B4825gP1rlPG3hnV/EF19qvdXt7ZGXaSdPG1Dj3bPvkk%2BwFdnc6bbWaFZLcrn%2B6xqnBpWjz5me3kdl5x5mefp3rWnj69J6PUyqYfB4qDVSN1/XW55rYfAj4b2yz3XiVp9fmmZf3dvGYkjxknJVu%2BRjjtWxo/wR8A29ubbT/h7FGpbcv2q6cMvtgMB%2BlegadBo8X/H1YTnH3XYHA9gBx7%2BvNaq3Gnqmyx2Pj%2BF8JXowzvGbqX4nz9bh7Jk7KgvuX52/M47w78MfDOloUXQrKLL5IMfmY4Hdjn8M4/Oun03QtBtAoMEAUDBjPyL%2BQPFTS2etaohisLKEJnlUkA2/j39fxqqfDOvA7ns7gnP3UGcn%2Bv6UPNMZUd22a0coyilBRjGMWvS50FpNosG2O2htMBsj5ASB6DOa1bfxBZocxsqbeo2gEVw0cQSRo5GKSKcMpBXH51atb6ztV5nHAycyAmnHGt7s0nllK1oncx%2BL2jO5zKykdM4H58VLH4qeY4gg2c8scsxH5muR0/xZ4ckk8u5mlJHbYRn9BW3p3iWzUbtN0SRj/tkHP8q6aeKUup5tfARp/wDLt3%2B5HQWs15dN5mxggGTlST%2BlX7fVoEU77jBX%2BHZzWLFrHiHUpAplSBeyAgkD6f56VFc61dq7WyhPMTG7YoAX8%2BtdcK9tjzJYWVR2dvk/%2BAdTc6vGLEMb8qzjAVMn8OnFQahNqMloiWQE6g5Vl2lV6ZBGMdv1rCtpL%2B%2BCzI0SvxvYyA5z221eurmbT7WS8ntZJRCmSBIcKMDjAP05r0KNR1Op42Koqj01RtT674Xex2anZxxBk%2B6q7lP15x19DXO%2BI/GfhWCzj0u1jWaaIB1YDJDe5HHbtnmuZ1u5uvEt5HFJp0tujrlJDMoXr6fQev8AhVOfw4YCLTTo1eXeQ5XOWGM9uv05r2KFKLV2z5HHYqrF8sYnoHhfxre2FjsuNXUeap8tGZjknPJOffpzW94d1m1spDFdeJ4syqW2gnaCTkjlufp2ryAvLbF7O5iYyONkcbL949sZOQDW74Q8PaheSm2laC2KEeWJXBkI9QCeR/nFegoJdTw5VpT5ly77nqqPb3U6b9XW8tzyipkbeM4JBPbtxV8%2BIND0u3MGn2Ky/I2c4UR4BPPOa4mTSfEfn29rb3drDFEcRTQpudsZyWwduT349%2BM0/WvBfieOZZbrxSokmBUIsYVgMcthMY/Hr0rroKFtzxcwVS1lEt3muTXincjy5O8tvwB%2BHFctd3SR3rIXEbrJgAg8H1x3PI5NdDZXKaJCvh5H8/zOWmaEnbnPcnAPb8KrN4bl1C%2BM0QUKG3vPPuyc%2BgJ%2Bbp7V61GrGB8jjMHOuytYXN5fTJCHnkdzmONEOOO%2BBwa6rw94V1O8lY3WmKqycKJSBj34qXStGihwHlhZsbmk8sZA%2BhbBrprO6jlVLGxm%2BZCAXKjcffb2/TFFTGNfCZYfJ4NLm1MqfQodGhWONc44YRQkAnngHOBTjZtdalE800kfA/dCRju9e%2BP0robrSra1iS8k/fSZ%2BTeuVU/%2BzHn6VnQG6aVrN5Uw55WID/OKhYqT3NJ5bTWyL1rpdjbTC%2BjmaRgvDbzgH05q3mdovKWBCueigVB5UdsofULoqq8AHHWnxnTpX%2BWRs9izY/XNUqre5jLDRpxslYfHFLGgQJ09WNFPWW12j/TV6f3/AP69FPnOf6ufnBpuleKhIHvfFk7LnO2OFQfz2n%2BVaTyWsa%2BRPqzSMM7kRyzY9eAf6VK9parpqzLbRh8N84QZ796iMstvbRmCRkJPOw4z%2BVfy37adRq5/pC4xpQbXT%2Bt3c2fCsVhdKklut%2BmPmUy78f4V2VjbEgGLCgD5nYcmsHw87yaVC8jFmwOScmrLSyidkErY9N1dtF8kUfO4ybrVXbT8ToVu7WxPybJGI644ptzryKPl2/mR%2BlYMDubgAuTz3NQqS00m4k/vD1%2BtbSxM4rQ444Om3d6mvLeLetiSIlQeMDGTSW2lwsxcMUJ98VIWYW0QBIHp%2BVMkZmu1VmJGOhPuKpd3qQpW92OhYgLRSrFJcZVRjOetT2unadNdELHhPTuTVObghhwc9RWtpckgKMJGB3dQfetqUuZpM560nThdGlpnhe5iUTxbkU8jIPPv7Vt2vheKdEP2u5znP7m624Pryam0mGJ7cM8SktyxK9ansJJFuCquQN3QGvYoUqemh8zisTXd3fYraj4XZhuXVpnIH3ZYFb9cVjXHw7a6R5JdBjkc9J8lD%2BXT9K7W4RGVdyg8dx7VLptrbbCfs8eef4BW8qUHKz/r7zmo4/E0qfNF/wBfKx5rL4L1axBQJGV6YAQkfTIqS08GXkoDSLcqueXIyFHrjIz%2BFdnq4CuQoxz2qHSba3KPIbdC2fvbBmp9jCM7bncszxNSlfQxdIsbHQ2aVLqYmU7VMkPynHQ88nvwKt6bqE2mXk01n5YaYfvEWISfTJ65p2pQwiK8lEK7lb5W2jI4NVrd3%2ByCTedwbhs8iuukrSSWliJL2kXKet9zctby%2BZDDfQJKJVwY44FRiMY4Iyc1U1fT7S6RLMWVwsZAClJ84P1PTr61btifPj56rk%2B5zUyyyrdR7ZGHzN0avTpwt1PErVVBXSLNr4Ps200PFp7yRRgbzIoYA%2BpxUUPh65gDCx05N8fIyADz9eO9b%2BgAPbTBxnMZznvwaZawQ/a54vJXb5QO3aMZxmvRpNpp3Pn6/LPmTRx2qfD6XUUbfYxJcp1ZmzwDywPQ/ifqKydJtIBqklxrd55YQDY0aq7Fvfb39OO/0rtblme42sSQ0JZge5DjB%2BtchqP/AB8iP%2BFi25exr1cPVclqfM43DwhJuJPrPinSCHttKie4u2JC3BXOznjAwMDGecmr2j6ncWtq1/q%2Bp7pdgCefICce2CP1/rUOjWNisVo62cQZ0G8iMZbnv61X8ZQxJYyxJEoUJkKF4HNerRjHsfKY2dRdRdT8WnUroQ2%2BoxMvAH7tgB3wOcH9K3tF1NZY00%2B/VAHbG9Iju9yDkgHj8AT60ml21tbaQjW1ukZNhEcogHJdMnir/hx3bxCqsxI%2BzA4J74rrk1GOiPHpwcql2zrPD9h4ft7GOOGDcWO5nlOcn8sZrSDW1krJpgLk9SvGfwFRzRRGxLmNSQ5AJHI4FPsCXh3OcnPU1zXudrilG5dtoZfKWScooTk7zzioJLqWMkW6L5jHnHGRU0RP2YjPSPIpmjqsk7%2BYob5M8jPanchwuUrqLUdVk2XqFk4XAxj2A/z/AEqw3hwJYrC0EUcI42luv9e9bkCILaYhQNiqU4%2B706elV9U/1Jf%2BLYOe/aq55GEoRRQXSdJjUIMnAxwgoqVACOR3P86Kvnn3MOSn2P/Z";
        String decode = URLDecoder.decode(a);
        System.out.println(decode);

    }


    @PostMapping("/20002/test")
    //手势验证码
    public R GestureVerificationCodeTest(@RequestBody String data) {
        String data_sub = data.substring(22, data.length());
//        log.info("data_sub:{}", data_sub);
        String picMd5 = DigestUtils.md5DigestAsHex(data_sub.getBytes());
        log.info("picMd5:{}", picMd5);
        SysImg sysImg = sysImgMapper.selectOne(Wrappers.<SysImg>lambdaQuery().eq(SysImg::getPicMd5, picMd5));
        if (ObjectUtil.isNotNull(sysImg)) {
            log.info("记录一下这条数据被查询的次数");
            sysImg.setFindCount(sysImg.getFindCount() + 1);
            sysImgMapper.updateById(sysImg);
            return R.ok(sysImg.getX1y1());
        } else {
            SysImg sysImgNew = new SysImg(picMd5, data, null, new Date());
            sysImgMapper.insert(sysImgNew);
        }
        return R.failed();
    }

    @PostMapping("/20002")
    //手势验证码
    public R GestureVerificationCode(@RequestBody String data) {
        //处理原始md5
        data = data.replaceAll("=", "");
        try {
//            data = URLEncoder.encode(data, "UTF-8");
            data = URLDecoder.decode(data);
            data = URLDecoder.decode(data);
        } catch (Exception e) {
            log.info("解码失败");
        }
        System.out.println(DigestUtils.md5DigestAsHex(data.getBytes()));
        String data_sub = data.substring(22, data.length());
//        log.info("data_sub:{}", data_sub);
        String picMd5 = DigestUtils.md5DigestAsHex(data_sub.getBytes());
        log.info("picMd5:{}", picMd5);
        SysImg sysImg = sysImgMapper.selectOne(Wrappers.<SysImg>lambdaQuery().eq(SysImg::getPicMd5, picMd5).isNotNull(SysImg::getX1y1));
        if (ObjectUtil.isNotNull(sysImg)) {
            log.info("记录一下这条数据被查询的次数");
            sysImg.setFindCount(sysImg.getFindCount() + 1);
            sysImgMapper.updateById(sysImg);
            return R.ok(sysImg.getX1y1());
        }
        SysImg sysImgDb = sysImgMapper.selectOne(Wrappers.<SysImg>lambdaQuery().eq(SysImg::getPicMd5, picMd5));
        if (ObjectUtil.isNull(sysImgDb)) {
            SysImg sysImgNew = new SysImg(picMd5, data, null, new Date());
            sysImgMapper.insert(sysImgNew);
            log.info("插入当前请求数据的日志 sysImg:[{}]", sysImgNew);
        }
        if (send2002(data, data_sub, picMd5)) {
            return R.failed();
        }
        return get2002(data, picMd5);
    }

    private R get2002(String data, String picMd5) {
        // "http://laiket.cn:7777/get?user=2082914704&pwd=hc20021122&type=20002&do=get&picMD5=%s" % (md5_img),
        try {
            Thread.sleep(3000L);
            String get_url = String.format("http://laiket.cn:7777/get?user=%s&pwd=%s&type=%s&do=get&picMD5=%s", laiTeKe.getLai_te_ke_username(),
                    laiTeKe.getLai_te_ke_password(), "20002", picMd5);
            HttpResponse execute = HttpRequest.get(get_url).header("Encoding", "UTF8").timeout(2000).execute();
            log.info("第一次请求结果是:{}", URLDecoder.decode(execute.body(), "utf-8"));
            if (execute.body().contains("1001")) {
                R r = buildPicImg(execute.body(), picMd5, data);
                if (r.getCode() == CommonConstants.SUCCESS) {
                    return r;
                }
            } else {
                Thread.sleep(2000L);
                execute = HttpRequest.get(get_url).header("Encoding", "UTF8").timeout(2000).execute();
                if (!execute.body().contains("1001")) {
                    return R.failed();
                }
//            code:1001,msg:x1,y1|x2,y2|x3,y3|x4,y4
                R r = buildPicImg(execute.body(), picMd5, data);
                return r;
            }
        } catch (Exception e) {
            log.info("数据流没有对于的值");
        }
        return R.failed();
    }


    private boolean send2002(String data, String data_sub, String picMd5) {
        String send_url = String.format("http://laiket.cn:7777/get?user=%s&pwd=%s&type=%s&do=send&picMd5=%s", laiTeKe.getLai_te_ke_username(),
                laiTeKe.getLai_te_ke_password(), "20002", picMd5);
        try {
            HttpResponse execute = HttpRequest.post(send_url).body(data_sub).header("Encoding", "UTF8").timeout(2000).execute();
            System.out.println(execute.body());
            if (!execute.body().contains("1001")) {
                execute = HttpRequest.post(send_url).body(data).header("Encoding", "UTF8").timeout(2000).execute();
                if (!execute.body().contains("1001")) {
                    return true;
                }
            }
        } catch (Exception e) {
            HttpResponse execute = HttpRequest.post(send_url).body(data).header("Encoding", "UTF8").timeout(2000).execute();
            if (!execute.body().contains("1001")) {
                return true;
            }
        }
        return false;
    }


    private R buildPicImg(String body, String picMd5, String data) throws Exception {
        String msg = URLDecoder.decode(body, "utf-8");
        msg = msg.substring("code:1001,msg:".length());
        System.out.println(msg);
        String[] msg_split = msg.split("\\|");
        if (msg_split.length == 4) {
            log.info("查询数据为md5的值为:{},数据为", picMd5, msg_split);
            int x1 = Integer.valueOf(msg_split[0].split(",")[0]);
            int x2 = Integer.valueOf(msg_split[1].split(",")[0]);
            int x3 = Integer.valueOf(msg_split[2].split(",")[0]);
            int x4 = Integer.valueOf(msg_split[3].split(",")[0]);
            int y1 = Integer.valueOf(msg_split[0].split(",")[1]);
            int y2 = Integer.valueOf(msg_split[1].split(",")[1]);
            int y3 = Integer.valueOf(msg_split[2].split(",")[1]);
            int y4 = Integer.valueOf(msg_split[3].split(",")[1]);
            X1y1x2y2x3y3x4y4 x1y1x2y2x3y3x4y4 = new X1y1x2y2x3y3x4y4(x1, x2, x3, x4, y1, y2, y3, y4);
            SysImg sysImgDb = sysImgMapper.selectOne(Wrappers.<SysImg>lambdaQuery().eq(SysImg::getPicMd5, picMd5));
            if (ObjectUtil.isNotNull(sysImgDb)) {
                SysImg sysImgNew = new SysImg(sysImgDb.getId(), picMd5, data, JSON.toJSONString(x1y1x2y2x3y3x4y4), 0, new Date());
                sysImgMapper.updateById(sysImgNew);
            }
            return R.ok(JSON.toJSONString(x1y1x2y2x3y3x4y4));
        }
        return R.ok();
    }

}


