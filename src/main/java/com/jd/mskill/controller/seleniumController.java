package com.jd.mskill.controller;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jd.mskill.config.R;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URLEncoder;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@Slf4j
@RequestMapping("/selenium")
public class seleniumController {

    @PostMapping("/test")
    //手势验证码
    public R GestureVerificationCodeTest(@RequestBody String data) {

        return R.ok();
    }

    public static final String chrome_path = "E:\\driver\\chromedriver.exe";

    public static void main(String[] args) throws Exception {


        System.setProperty("webdriver.chrome.driver", chrome_path);
        Map<String, String> mobileEmulation = new HashMap<>();
        mobileEmulation.put("deviceName", "Nexus 5");
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setCapability("networkConnectionEnabled", true);
        chromeOptions.setExperimentalOption("w3c", false);
        chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);
        WebDriver driver = new ChromeDriver(chromeOptions);
        driver.get("https://plogin.m.jd.com/mreg/index");

        ((ChromeDriver) driver).findElementByXPath("//a[@class='protocol-btn agreement']").click();
        ((ChromeDriver) driver).findElementByXPath("//input[@placeholder='请输入手机号']").sendKeys("18408282245");
        ((ChromeDriver) driver).findElementByXPath("//a[@class='btn active']").click();
        Thread.sleep(1000);
        String cpc_img = ((ChromeDriver) driver).findElementById("cpc_img").getAttribute("src");
        cpc_img = cpc_img.replaceAll("\\+", "%2B");
        String cpc_img_encode = URLEncoder.encode(cpc_img, "utf-8");
        HttpResponse execute = HttpRequest.post("http://xuhao.vaiwan.com/laiteke/20002").body(cpc_img_encode).execute();

        String body = execute.body();
        JSONObject jsonObject = JSON.parseObject(body);
        Object data = jsonObject.get("data");
        JSONObject x1_y1 = JSON.parseObject(data.toString());
        Double x1 = Double.valueOf(x1_y1.get("x1").toString());
        Double x2 = Double.valueOf(x1_y1.get("x2").toString());
        Double x3 = Double.valueOf(x1_y1.get("x3").toString());
        Double x4 = Double.valueOf(x1_y1.get("x4").toString());

        Double y1 = Double.valueOf(x1_y1.get("y1").toString());
        Double y2 = Double.valueOf(x1_y1.get("y2").toString());
        Double y3 = Double.valueOf(x1_y1.get("y3").toString());
        Double y4 = Double.valueOf(x1_y1.get("y4").toString());

        System.out.println("成功");

        Double k1 = (y1 - y2) / (x1 - x2);
        Double k2 = (y2 - y3) / (x2 - x3);
        Double k3 = (y3 - y4) / (x3 - x4);
        Double b1 = (y2 * x1 - y1 * x2) / (x1 - x2);
        Double b2 = (y3 * x2 - y2 * x3) / (x2 - x3);
        Double b3 = (y4 * x3 - y3 * x4) / (x3 - x4);


        List<Double> x_array = new ArrayList<>();
        List<Double> y_array = new ArrayList<>();
        x_array.add(x1);
        y_array.add(y1);
        //计算第一条线
        if (x1 - x2 < 0) {
            double b_size = (x2 - x1) / 20;
            for (int i = 0; i < b_size; i++) {
                double x_t = x1 + 20 * i;
                x_array.add(x_t);
                y_array.add(k1 * x_t + b1);
            }
        } else {
            double b_size = (x1 - x2) / 20;
            for (int i = 0; i < b_size; i++) {
                double x_t = x1 - 20 * i;
                x_array.add(x_t);
                y_array.add(k1 * x_t + b1);
            }
        }
        x_array.add(x2);
        y_array.add(y2);
        //计算第二条线
        if (x2 - x3 < 0) {
            double b_size = (x3 - x2) / 20;
            for (int i = 0; i < b_size; i++) {
                double x_t = x2 + 20 * i;
                x_array.add(x_t);
                y_array.add(k2 * x_t + b2);
            }
        } else {
            double b_size = (x2 - x3) / 20;
            for (int i = 0; i < b_size; i++) {
                double x_t = x2 - 20 * i;
                x_array.add(x_t);
                y_array.add(k2 * x_t + b2);
            }
        }
        x_array.add(x3);
        y_array.add(y3);
        //计算第三条线
        if (x3 - x4 < 0) {
            double b_size = (x4 - x3) / 10;
            for (int i = 0; i < b_size; i++) {
                double x_t = x3 + 10 * i;
                x_array.add(x_t);
                y_array.add(k3 * x_t + b3);
            }
        } else {
            double b_size = (x3 - x4) / 10;
            for (int i = 0; i < b_size; i++) {
                double x_t = x3 - 10 * i;
                x_array.add(x_t);
                y_array.add(k3 * x_t + b3);
            }
        }
        x_array.add(x4);
        y_array.add(y4);
        List<Integer> x_array_int = x_array.stream().map(it -> it.intValue()).collect(Collectors.toList());
        List<Integer> y_array_int = y_array.stream().map(it -> it.intValue()).collect(Collectors.toList());
        List<Integer> x_array_index = new ArrayList<>();
        for (int i = 0; i < x_array_int.size(); i++) {
            if (i != x_array_int.size() - 1) {
                if (x_array_int.get(i).equals(x_array_int.get(i + 1))) {
                    x_array_index.add(i);
                }
            }
        }
        List<Integer> x_array_int_new = new ArrayList<>();
        List<Integer> y_array_int_new = new ArrayList<>();

        for (int i = 0; i < x_array_int.size(); i++) {
            if (!x_array_index.contains(i)) {
                x_array_int_new.add(x_array_int.get(i));
                y_array_int_new.add(y_array_int.get(i));
            }
        }
        WebElement trackLine_element = ((ChromeDriver) driver).findElementById("trackLine");
        Actions actions = new Actions(driver).moveToElement(trackLine_element, x1.intValue(), y1.intValue()).clickAndHold();
        for (int i = 0; i < x_array_int_new.size() - 1; i++) {
            actions.moveToElement(trackLine_element, x_array_int_new.get(i + 1), y_array_int_new.get(i + 1));
        }
        actions.release().pause(1000).perform();
        System.out.println("成功");
    }


}
