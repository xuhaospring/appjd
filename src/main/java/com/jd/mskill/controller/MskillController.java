package com.jd.mskill.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jd.mskill.dto.Eid;
import com.jd.mskill.dto.EidRe;
import com.jd.mskill.dto.SignDaily;
import com.jd.mskill.dto.SignVO;
import com.jd.mskill.threadClass.*;
import com.jd.mskill.utils.FileContent;
import com.jd.mskill.utils.UUIDUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@RestController
@RequestMapping
@Slf4j
public class MskillController {

    static ExecutorService pool = Executors.newFixedThreadPool(30);


    private static Process proc = null;//java进程类

    @GetMapping("/yy")
    public String yy() throws Exception {
        exec(1);
        return "预约成功";
    }

    @GetMapping("/ms")
    public String ms() throws Exception {
        exec(2);
        return "偷看。个人去看日志";
    }

//    public static void main(String[] args) throws Exception {
//        exec(1);
//    }

    @PostMapping("/token")
    public String token() throws Exception {
        String tokens = exec(0);
        log.info("tokenInfo");
        return tokens;
    }


    @PostMapping("/sign")
    public String sign(@RequestBody SignVO signVO) throws Exception {
        // String skuParam, String currentOrderFunctionId, String uuid
        String initConfig = FileContent.readFileContent("E:/hook/init/initConfig").trim();
        String python_home = buidInid(initConfig, "python_home");
        String sign_home = buidInid(initConfig, "sign_home");
        if(StrUtil.isBlank(signVO.getClientVersion())){
            signVO.setClientVersion(buidInid(initConfig, "clientVersion"));
        }
        String signs = getSign(signVO.getSkuParam(), python_home, sign_home, signVO.getCurrentOrderFunctionId(), signVO.getUuid(), signVO.getClientVersion());
        log.info("signs={}", signs);
        return signs;
    }

    @PostMapping("/sign_eid")
    public String sign_eid(@RequestBody Eid eid) throws Exception {
        String uuid= "044bff46f4484f2d";
        String function ="ccSignInNew";
        String version= "9.2.0";
        // String skuParam, String currentOrderFunctionId, String uuid
        String initConfig = FileContent.readFileContent("E:/hook/init/initConfig").trim();
        String python_home = buidInid(initConfig, "python_home");
        String sign_home = buidInid(initConfig, "sign_home");
        String sign_eid = buidInid(initConfig, "sign_eid");
        HashMap hashMap = JSON.parseObject(sign_eid, HashMap.class);
        hashMap.put("eid",eid.getEid());
        String body = JSON.toJSONString(hashMap);
        String signs = getSign(body, python_home, sign_home, function, uuid, version);
        log.info("signs={}", signs);
        String st =  signs.split("&")[0].substring(signs.split("&")[0].indexOf("=") + 1);
        String sign= signs.split("&")[1].substring(signs.split("&")[1].indexOf("=") + 1);
        String sv= signs.split("&")[2].substring(signs.split("&")[2].indexOf("=") + 1);
        EidRe eidRe = new EidRe(st,sign,sv,body,uuid,version,function);
        return JSON.toJSONString(eidRe);
    }


    @PostMapping("/eid")
    public String eid(@RequestBody Eid eid) throws Exception {
        String eid_t = "{\"childActivityUrl\":\"openapp.jdmobile://virtual?params={\\\"category\\\":\\\"jump\\\",\\\"des\\\":\\\"couponCenter\\\"}\",\"eid\":\"eidA04e0812228s9+sREUrrhRpOLhtMaKKJj7PrERcsOuus9C9qCjWy1dgDfdRA+I+vVYkxpeJWM20B0NocrEmgPeJq//Rva83eqYuFw5RAdGgX3NH9B\",\"monitorRefer\":\"appClient\",\"monitorSource\":\"cc_sign_android_index_config\",\"pageClickKey\":\"Coupons_GetCenter\",\"shshshfpb\":\"kXUzseF6nTAy4r/+AQFur0F7mzOwjXSEKTw0aqR9OgHNz7b33db+b7ByoVbm0uLs6\"}";
        HashMap hashMap = JSON.parseObject(eid_t, HashMap.class);
        hashMap.put("eid", eid.getEid());
        // String skuParam, String currentOrderFunctionId, String uuid
        log.info("{}", hashMap);
        return JSON.toJSONString(hashMap);
    }

    @PostMapping("/test")
    public String test() throws Exception {
        String initConfig = FileContent.readFileContent("E:/hook/init/initConfig").trim();
        String  pa = "{\"childActivityUrl\":\"openapp.jdmobile://virtual?params={\\\"category\\\":\\\"jump\\\",\\\"des\\\":\\\"couponCenter\\\"}\",\"eid\":\"eidAb1b18121a5s9GXQK8C0XRlCPbAm7Rrj8Vhl0DGiCjg6r0FPjlKHFkC3WzdmcPXoQDzRpX3aTLhz3KfF6/Iy/2aNgFcjRCnbuPyNGlh04xBdHfENW\",\"monitorRefer\":\"appClient\",\"monitorSource\":\"cc_sign_android_index_config\",\"pageClickKey\":\"Coupons_GetCenter\",\"pin\":\"c5cc6adc2eef5503c04bf02ec1396f5f\",\"sessionId\":\"\",\"shshshfpb\":\"uGaLf3GMKizDRVmv/ExNjahgAIdlEOtDqAL3EgXginTmH0DpAnCmZji8a00elb8qdOZYTAuqlIO7+Ymo9OgnIxA==\",\"verifyToken\":\"\"}";
        String sign_daily = buidInid(initConfig, "sign_daily").trim();
        String signs = getOrignSign("ccSignInNew", "044bff46f4484f2d", "http://127.0.0.1:7890/sign", sign_daily,"9.2.0");
       // String ccSignInNew = getOrignSign("ccSignInNew", "044bff46f4484f2d", "http://127.0.0.1:7890/sign",JSON.toJSONString(pa));
        return signs;
    }

    @PostMapping("/sign_daily")
    public String sign_daily(@RequestBody SignDaily signDaily) throws Exception {
        String initConfig = FileContent.readFileContent("E:/hook/init/initConfig").trim();
        if (StrUtil.isBlank(signDaily.getSkuParam())) {
            signDaily.setSkuParam(buidInid(initConfig, "sign_daily").trim());
        }
        if(StrUtil.isBlank(signDaily.getCurrentOrderFunctionId())){
            signDaily.setCurrentOrderFunctionId("ccSignInNew");
        }
        if(StrUtil.isBlank(signDaily.getClientVersion())){
            signDaily.setClientVersion("9.2.0");
        }
        if(StrUtil.isBlank(signDaily.getUuid())){
            signDaily.setUuid("c09e8cd24d9a6d2d");
        }
        String contentType = buidInid(initConfig, "contentType");
        String python_home = buidInid(initConfig, "python_home");
        log.debug("----");
        String sign_home = buidInid(initConfig, "sign_home");
//        String signs = getSign(signDaily.getSkuParam(), python_home, sign_home, signDaily.getCurrentOrderFunctionId(),
//                signDaily.getUuid(), signDaily.getClientVersion());
        String signs = getOrignSign(signDaily.getCurrentOrderFunctionId(), signDaily.getUuid(), "http://127.0.0.1:7890/sign", signDaily.getSkuParam(),signDaily.getClientVersion());
        log.info("获取签到信息为{}", signs);
        if(StrUtil.isBlank(signs)){
           return "失败";
        }
        log.info("开始执行post方法");
//        api.m.jd.com/client.action?functionId=ccSignInNew&clientVersion=9.2.0&client=android&uuid=c09e8cd24d9a6d2d&st=1616416382077&sign=665be2d66170a93abea0ca8856f28fd2&sv=101
        //st=1616390552665
        //sign=6c0bb24e8a72d5d36a57b1470c104d48
        //sv=111
        String[] split = signs.split("&");
        String st =  signs.split("&")[0].substring(signs.split("&")[0].indexOf("=") + 1);
        String sign= signs.split("&")[1].substring(signs.split("&")[1].indexOf("=") + 1);
        String sv= signs.split("&")[2].substring(signs.split("&")[2].indexOf("=") + 1);
        String api_url = String.format("api.m.jd.com/client.action?functionId=ccSignInNew&clientVersion=%s&client=android&uuid=c09e8cd24d9a6d2d&st=%s&sign=%s&sv=%s",
                signDaily.getClientVersion(), st, sign, sv);
        log.info(api_url);
        HttpResponse execute = HttpRequest.post(api_url).form("body", signDaily.getSkuParam()).cookie(signDaily.getCk()).contentType(contentType).timeout(2000).execute();
        String body = execute.body();
        return body;
    }

    /**
     * 执行*.py文件
     */
    public synchronized String exec(Integer isYuyue) throws Exception {
        String initConfig = FileContent.readFileContent("E:/hook/init/initConfig").trim();
//        String currentOrderParamConfig = FileContent.readFileContent("E:/hook/init/currentOrderParamConfig").trim();
//        String submitOrderParamConfig = FileContent.readFileContent("E:/hook/init/submitOrderParamConfig").trim();
        // sku 默认为100000000000
        String python_home = buidInid(initConfig, "python_home");
        String submitOrderParamConfig = buidInid(initConfig, "submitOrderParamConfig");
        String sign_home = buidInid(initConfig, "sign_home");
        String currentOrder_functionId = buidInid(initConfig, "currentOrder_functionId");
        String appoint_functionId = buidInid(initConfig, "appoint_functionId");
        String submitOrder_functionId = buidInid(initConfig, "submitOrder_functionId");
        String cps_functionId = buidInid(initConfig, "cps_functionId");
        String genToken_functionId = buidInid(initConfig, "genToken_functionId");
        String build = buidInid(initConfig, "build");
        String client = buidInid(initConfig, "client");
        Long mskill_time = Long.valueOf(buidInid(initConfig, "mskill_time"));
        Integer ratio = Integer.valueOf(buidInid(initConfig, "ratio"));
        String clientVersion = buidInid(initConfig, "clientVersion");
        Integer work_count = Integer.valueOf(buidInid(initConfig, "work_count"));
        String bash_url_currentOrder = buidInid(initConfig, "bash_url_currentOrder");
        String bash_url_appoint = buidInid(initConfig, "bash_url_appoint");
        String bash_url_submitOrder = buidInid(initConfig, "bash_url_submitOrder");
        String bash_url_genToken = buidInid(initConfig, "bash_url_genToken");
        String bash_url_cps = buidInid(initConfig, "bash_url_cps");
//        String cookie = buidInid(initConfig, "cookie");
        String contentType = buidInid(initConfig, "contentType");
        String sign_url = buidInid(initConfig, "sign_url");
        String cps = buidInid(initConfig, "cps");
        String appointParamConfig = buidInid(initConfig, "appointParamConfig");
        String genTokenParamConfig = buidInid(initConfig, "genTokenParamConfig");
        String[] sku_ids = buidInid(initConfig, "sku_id").split(";");
        work_count = work_count * sku_ids.length * ratio;
        List<String> skus = Arrays.asList(sku_ids);
        List<String> currentOrderParamConfigs = new ArrayList<>();
        List<String> cookies = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            String currentOrderParamConfig = buidInid(initConfig, "currentOrderParamConfig" + i).trim();
            if (StrUtil.isNotBlank(currentOrderParamConfig)) {
                currentOrderParamConfigs.add(currentOrderParamConfig);
            }
            String cookie = buidInid(initConfig, "cookie" + i).trim();
            if (StrUtil.isNotBlank(cookie)) {
                cookies.add(cookie);
            }
        }
        List<JSONObject> tokens = new ArrayList<>();
        for (String sku : skus) {
//            https://item.jd.com/100009958327.html
            try {
                String format = String.format("https://item-soa.jd.com/getWareBusiness?callback=jQuery8982587&skuId=%s", sku);
                String result = HttpUtil.get(format);
                int result1 = result.indexOf("buyTime");
                Date buyTime = new Date();
                if (result1 == -1) {
                    buyTime = new Date(new Date().getTime() + 10000L);
                } else {
                    String buyTimeStr = result.substring(result1 + 10, result1 + 26);
                    buyTime = DateUtil.parse(buyTimeStr, "yyyy-MM-dd HH:mm");
                }
                if (StrUtil.isBlank(sku)) {
                    continue;
                }
                Integer index = 0;
                if (isYuyue == 0) {
                    String uuid = UUIDUtils.generate_uuid("f5d23d2a30f03be8".length());
                    String skuParam = genTokenParamConfig.replaceAll("100000000000", sku);
                    String signs = getOrignSign(genToken_functionId, uuid, sign_url, skuParam,clientVersion);
                    if (StrUtil.isBlank(signs)) {
                        log.info("签证失败，快去联系耗子");
                        continue;
                    }
                    long before = System.currentTimeMillis();
                    List<Future<String>> futures = new ArrayList<>();
                    for (int j = 0; j < cookies.size(); j++) {
                        List<String> signsArr = Arrays.asList(signs.split("&"));
                        Map<String, String> params = buidParamMap(build, client, uuid, clientVersion, signsArr);
                        Token tokenTask = new Token(bash_url_genToken, skuParam, cookies.get(j), contentType, params, sku);
                        Future<String> submit = pool.submit(tokenTask);
                        futures.add(submit);
//                        String body = submit.get();
//                        JSONObject jsonObject = JSON.parseObject(body);
//                        System.out.println(System.currentTimeMillis() - before);
//                        tokens.add(jsonObject);
                    }
                    for (Future<String> future : futures) {
                        String body = future.get();
                        JSONObject jsonObject = JSON.parseObject(body);
                        tokens.add(jsonObject);

                    }
                    System.out.println(System.currentTimeMillis() - before);
                    System.out.println("结束");
                }
                if (isYuyue == 1) {
                    for (int j = 0; j < cookies.size(); j++) {
                        String uuid = UUIDUtils.generate_uuid("f5d23d2a30f03be8".length());
                        String skuParam = appointParamConfig.replaceAll("100000000000", sku);
                        String signs = getOrignSign(appoint_functionId, uuid, sign_url, skuParam,clientVersion);
                        List<String> signsArr = Arrays.asList(signs.split("&"));
                        log.debug("");
                        Map<String, String> params = buidParamMap(build, client, uuid, clientVersion, signsArr);
                        Appoint task = new Appoint(bash_url_appoint, skuParam, cookies.get(j), contentType, params);
                        pool.submit(task);
                    }
                } else if (isYuyue == 2) {
                    for (int i = 0; i < work_count; i++) {
                        for (int j = 0; j < cookies.size(); j++) {
                            String uuid = UUIDUtils.generate_uuid("f5d23d2a30f03be8".length());
                            String skuParam = currentOrderParamConfigs.get(j).replaceAll("100000000000", sku);
                            String cookie = cookies.get(j);
                            String signs = getOrignSign(currentOrder_functionId, uuid, sign_url, skuParam,clientVersion);
                            if (StrUtil.isBlank(signs)) {
                                log.info("签证失败");
                                continue;
                            }
//                String signs = getSign(skuParam, python_home, sign_home, currentOrder_functionId, uuid, clientVersion);//本地签名
//              st=1611721236554&sign=ba2946d4dcb6a1ec20fdf9d62230e803&sv=111
                            List<String> signsArr = Arrays.asList(signs.split("&"));
                            Map<String, String> params = buidParamMap(build, client, uuid, clientVersion, signsArr);
                            // public runParamThread(String bashUrl, String body, String cookie, String contentType, Map<String, String> params, String sku, org.slf4j.Logger log) {
                            SubmitOrder submitOrder = buidSubmitOrderClass(submitOrderParamConfig, python_home, sign_home, submitOrder_functionId, build, client, uuid, clientVersion, work_count, bash_url_submitOrder,
                                    cookie, contentType, sku, buyTime, sign_url,
                                    cps_functionId, bash_url_cps, cps, pool, mskill_time);
//                     new Thread(submitOrder).start();
//                     log.info("开始提交订单。。循环");
                            CurrentOrderThread task = new CurrentOrderThread(bash_url_currentOrder, skuParam, cookie, contentType, params, sku, buyTime, submitOrder, mskill_time);
                            pool.submit(task);
                        }
                    }
                }

            } catch (Exception e) {
                log.error("获取时间错误:{}", e);
            }
        }
        return JSON.toJSONString(tokens);
    }

    private String GetCpsMe(String cps_functionId, String build, String client, String uuid, String clientVersion, String bash_url_cps, String cookie, String contentType, String sign_url, String cps, ExecutorService poolcps) {
        try {
            String cpsJSON = JSON.toJSONString(cps);
            String signs = getOrignSign(cps_functionId, uuid, sign_url, cpsJSON,clientVersion);
            List<String> signsArr = Arrays.asList(signs.split("&"));
            Map<String, String> params = buidParamMap(build, client, uuid, clientVersion, signsArr);
            Cps task = new Cps(bash_url_cps, cpsJSON, cookie, contentType, params);
            Future<String> submit = poolcps.submit(task);
            String cpsPoolStr = submit.get();
            return cpsPoolStr;
        } catch (Exception e) {
            log.info("getcpsMe报错 e:{}", e);
        }
        return "";
    }


    private String getOrignSign(String currentOrder_functionId, String uuid, String sign_url, String skuParam,String version) {
        Map<String, Object> urlParams = new HashMap<>();
        urlParams.put("skuParam", skuParam);
        urlParams.put("currentOrderFunctionId", currentOrder_functionId);
        urlParams.put("uuid", uuid);
        urlParams.put("clientVersion",version);
        if (sign_url.contains(";")) {
            try {
                HttpResponse execute = HttpRequest.post(sign_url.split(";")[0]).body(JSON.toJSONString(urlParams)).contentType("application/json").timeout(20000).execute();
                log.info("第一次签证信息为：{}", execute.body());
                if (StrUtil.isNotEmpty(execute.body()) && execute.body().contains("sign")) {
                    return execute.body();
                } else {
                    log.error("第一签证失败。请联系耗子启动：{}", execute.body());
                    execute = HttpRequest.post(sign_url.split(";")[1]).body(JSON.toJSONString(urlParams)).contentType("application/json").timeout(20000).execute();
                    if (StrUtil.isNotEmpty(execute.body()) && execute.body().contains("sign")) {
                        return execute.body();
                    }
                }
            } catch (Exception e) {
                HttpResponse execute = HttpRequest.post(sign_url.split(";")[1]).body(JSON.toJSONString(urlParams)).contentType("application/json").timeout(20000).execute();
                if (StrUtil.isNotEmpty(execute.body()) && execute.body().contains("sign")) {
                    return execute.body();
                }

            }
        } else {
            try {
                HttpResponse execute = HttpRequest.post(sign_url).body(JSON.toJSONString(urlParams)).contentType("application/json").timeout(20000).execute();
                log.info("第一次签证信息为：{}", execute.body());
                if (StrUtil.isNotEmpty(execute.body()) && execute.body().contains("sign")) {
                    return execute.body();
                }
            } catch (Exception e) {
                log.error("签证全部挂掉。请速度联系耗子。不然抢购不到了");
            }
        }
        log.error("签证全部挂掉。请速度联系耗子。不然抢购不到了");
        return null;
    }

    private SubmitOrder buidSubmitOrderClass(String submitOrderParamConfig, String python_home, String
            sign_home, String submitOrder_functionId, String build, String client, String uuid,
                                             String clientVersion, Integer work_count, String bash_url_submitOrder, String cookie, String
                                                     contentType, String sku, Date buyTime, String sign_url,
                                             String cps_functionId, String bash_url_cps, String cps, ExecutorService poolcps, Long mskill_time) throws
            IOException, InterruptedException {
        if (StrUtil.isBlank(sku)) {
            return null;
        }
        String usid = GetCpsMe(cps_functionId, build, client, uuid, clientVersion, bash_url_cps, cookie, contentType, sign_url, cps, poolcps);
        log.debug("");
        String skuParam = submitOrderParamConfig.replaceAll("1q2w3e4r5t6y7u8i", sku);
//        skuParam = skuParam.replaceAll("1234567891011", String.valueOf(new Date().getTime()));
        String signs = getOrignSign(submitOrder_functionId, uuid, sign_url, skuParam,clientVersion);
//        String signs = getSign(skuParam, python_home, sign_home, submitOrder_functionId, uuid, clientVersion);
        log.debug("");
        List<String> signsArr = Arrays.asList(signs.split("&"));
        Map<String, String> params = buidParamMap(build, client, uuid, clientVersion, signsArr);
        for (int i = 0; i < work_count; i++) {
            log.debug("");
            SubmitOrder task = new SubmitOrder(bash_url_submitOrder, skuParam, cookie, contentType, params, sku, usid, buyTime, mskill_time);
            return task;
        }
        return null;
    }

    private static Map<String, String> buidParamMap(String build, String client, String uuid, String
            clientVersion, List<String> signsArr) {
        Map<String, String> params = new HashMap<>();
        params.put("build", build);
        params.put("client", client);
        params.put("uuid", uuid);
        params.put("clientVersion", clientVersion);
        for (String sign : signsArr) {
            if (sign.contains("st")) {
                params.put("st", sign.split("=")[1]);
                continue;
            }
            if (sign.contains("sign")) {
                params.put("sign", sign.split("=")[1]);
                continue;
            }
            if (sign.contains("sv")) {
                params.put("sv", sign.split("=")[1]);
                continue;
            }
        }
        return params;
    }

    private static String getSign(String skuParam, String python_home, String sign_home, String functionId, String
            uuid, String clientVersion) throws IOException, InterruptedException {
//        String result = "{\"CartStr\":{\"TheSkus\":[{\"Id\":\"100009958327\",\"num\":\"1\"}]},\"OrderStr\":{\"CoordType\":\"2\",\"Email\":\"\",\"Id\":707695531,\"IdArea\":49324,\"IdCity\":1930,\"IdProvince\":22,\"IdTown\":49399,\"Latitude\":\"30.531668\",\"Longitude\":\"104.095993\",\"Mobile\":\"uUCGVagccJ4hDz1DCsH4GA==\",\"Name\":\"242aBN3YnQU=\",\"Phone\":\"\",\"Where\":\"O1YzJh1E3pGXyO8ewxJeCpfzP0tscYwFoXZlfVIrft18/uhC/4d2P4e/jqhd8hxoLddQDHf4OpnKanKcu9hKSMF3rUeunqmH\",\"addressDefault\":true,\"addressDetail\":\"2OlaL+DUec5HhMhuI5ZgYSj/wlcMMCH8TuKjttJaAA2JRMeXQ6yEIg==\",\"addressUUID\":\"1011_910518214399463429\",\"areaCode\":\"\",\"bubbleTimeInfo\":{\"shipmentInfos\":1610266527095},\"closedTimeStamp\":\"\",\"closedVersion\":\"\",\"isNeedVirtualData\":true,\"isOpenPaymentPassword\":true,\"isShortPwd\":false,\"latitude\":\"\",\"longitude\":\"\",\"plusExposureCount\":6,\"plusFloorClosedTimeStamp\":\"1604749322501\",\"postCode\":\"\",\"redpacket_channel\":\"JD\",\"retainTipsNumber\":0},\"addressGlobal\":true,\"addressTotalNum\":3,\"appEID\":\"eidA173d812283saTEW2oEfoSsm7EyVu0iQwgkxFuBL+FRL/ypxaKNgzQ7RTrvjE1AF38mZMvZVIBVz9uvg1qDX7hqrAhSpKlZC21+GerZIt5g0pC3s2\",\"cartAdd\":{\"atmosphereList\":[{\"necessaryKey\":\"10\",\"showMsg\":\"3000元以上旗舰手机热卖榜第5名\",\"type\":4},{\"necessaryKey\":\"91%\",\"type\":5}],\"extFlag\":{},\"wareId\":\"100009958327\",\"wareNum\":\"1\"},\"decryptType\":\"2\",\"giftType\":0,\"hasDefaultAddress\":true,\"hasSelectedOTC\":\"0\",\"isLastOrder\":true,\"isRefreshOrder\":false,\"isSupportAllInvoice\":true,\"operationType\":0,\"otcMergeSwitch\":\"1\",\"settlementVersionCode\":1240,\"skuSource\":3,\"sourceType\":2,\"supportAllEncode\":true,\"wareId\":\"100009958327\",\"wareNum\":1}";
        String skuParamJSON = JSON.toJSONString(skuParam);
//        String a ="{\\\"CartStr\\\":{\\\"TheSkus\\\":[{\\\"Id\\\":\\\"100009958327\\\",\\\"num\\\":\\\"1\\\"}]},\\\"OrderStr\\\":{\\\"CoordType\\\":\\\"2\\\",\\\"Email\\\":\\\"\\\",\\\"Id\\\":707695531,\\\"IdArea\\\":49324,\\\"IdCity\\\":1930,\\\"IdProvince\\\":22,\\\"IdTown\\\":49399,\\\"Latitude\\\":\\\"30.531668\\\",\\\"Longitude\\\":\\\"104.095993\\\",\\\"Mobile\\\":\\\"uUCGVagccJ4hDz1DCsH4GA==\\\",\\\"Name\\\":\\\"242aBN3YnQU=\\\",\\\"Phone\\\":\\\"\\\",\\\"Where\\\":\\\"O1YzJh1E3pGXyO8ewxJeCpfzP0tscYwFoXZlfVIrft18/uhC/4d2P4e/jqhd8hxoLddQDHf4OpnKanKcu9hKSMF3rUeunqmH\\\",\\\"addressDefault\\\":true,\\\"addressDetail\\\":\\\"2OlaL+DUec5HhMhuI5ZgYSj/wlcMMCH8TuKjttJaAA2JRMeXQ6yEIg==\\\",\\\"addressUUID\\\":\\\"1011_910518214399463429\\\",\\\"areaCode\\\":\\\"\\\",\\\"bubbleTimeInfo\\\":{\\\"shipmentInfos\\\":1610266527095},\\\"closedTimeStamp\\\":\\\"\\\",\\\"closedVersion\\\":\\\"\\\",\\\"isNeedVirtualData\\\":true,\\\"isOpenPaymentPassword\\\":true,\\\"isShortPwd\\\":false,\\\"latitude\\\":\\\"\\\",\\\"longitude\\\":\\\"\\\",\\\"plusExposureCount\\\":6,\\\"plusFloorClosedTimeStamp\\\":\\\"1604749322501\\\",\\\"postCode\\\":\\\"\\\",\\\"redpacket_channel\\\":\\\"JD\\\",\\\"retainTipsNumber\\\":0},\\\"addressGlobal\\\":true,\\\"addressTotalNum\\\":3,\\\"appEID\\\":\\\"eidA173d812283saTEW2oEfoSsm7EyVu0iQwgkxFuBL+FRL/ypxaKNgzQ7RTrvjE1AF38mZMvZVIBVz9uvg1qDX7hqrAhSpKlZC21+GerZIt5g0pC3s2\\\",\\\"cartAdd\\\":{\\\"atmosphereList\\\":[{\\\"necessaryKey\\\":\\\"10\\\",\\\"showMsg\\\":\\\"3000元以上旗舰手机热卖榜第5名\\\",\\\"type\\\":4},{\\\"necessaryKey\\\":\\\"91%\\\",\\\"type\\\":5}],\\\"extFlag\\\":{},\\\"wareId\\\":\\\"100009958327\\\",\\\"wareNum\\\":\\\"1\\\"},\\\"decryptType\\\":\\\"2\\\",\\\"giftType\\\":0,\\\"hasDefaultAddress\\\":true,\\\"hasSelectedOTC\\\":\\\"0\\\",\\\"isLastOrder\\\":true,\\\"isRefreshOrder\\\":false,\\\"isSupportAllInvoice\\\":true,\\\"operationType\\\":0,\\\"otcMergeSwitch\\\":\\\"1\\\",\\\"settlementVersionCode\\\":1240,\\\"skuSource\\\":3,\\\"sourceType\\\":2,\\\"supportAllEncode\\\":true,\\\"wareId\\\":\\\"100009958327\\\",\\\"wareNum\\\":1}";
        String[] arguments = new String[]{python_home, sign_home, functionId, uuid,
                skuParamJSON, clientVersion};//实际上后两个参数传进入也没使用。当真正需要有参数传入时可以是利用这种方式传参
        proc = Runtime.getRuntime().exec(arguments);
        BufferedReader in = new BufferedReader(new InputStreamReader(proc.getInputStream(), "GBK"));
        String line = "";
        String returnResult = "";
        while ((line = in.readLine()) != null) {
            log.info("签证成功:{}", line);
            returnResult = line;
        }
        in.close();
        int re = proc.waitFor();//返回0：成功。其余返回值均表示失败，如：返回错误代码1：操作不允许，表示调用python脚本失败
        if (re == 0) {
            return returnResult;
        }
        return null;
    }

    private static String buidInid(String s, String key) {
        String value = "";
        for (String init : s.split("\n")) {
            if (init.contains(key) && !init.contains("#") && init.startsWith(key + "=")) {
                int i = init.indexOf("=");
                value = init.substring(i + 1, init.length());
            }
        }
        return value;
    }


}






