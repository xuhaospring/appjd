package com.jd.mskill.controller;


import com.jd.mskill.config.R;
import com.jd.mskill.mapper.SysGroupSmsMapper;
import com.jd.mskill.po.SysGroupSms;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@Slf4j
@RequestMapping("/sysSms")
public class SysSmsController {

    @Resource
    private SysGroupSmsMapper sysGroupSmsMapper;

    @PostMapping
    public R add(@RequestBody SysGroupSms sysGroupSms) {
        sysGroupSms.setId(null);
        sysGroupSmsMapper.insert(sysGroupSms);
        return R.ok();
    }


}
