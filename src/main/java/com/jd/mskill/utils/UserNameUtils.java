package com.jd.mskill.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class UserNameUtils {

    public static String getRandomPwd() {    //static修饰，可以不用创建对象，直接使用该方法。
        Random r = new Random();
        ArrayList<Character> pwlist = new ArrayList<Character>();
        for (int i = 0; i < 10; i++) {
            if (i < 3) {
                //依次生成3个数字，放在list里
                char num = (char) (48 + r.nextInt(10));   //ASCII码十进制代码48-57代表数字0-9
                pwlist.add(num);
            } else if (i < 6) {
                //依次生成3个大写字母，放在list里
                char upper = (char) (65 + r.nextInt(26)); //ASCII码十进制代码65-90代表大写字母A-Z
                pwlist.add(upper);
            } else if (i < 9) {
                //依次生成3个小写字母，放在list里
                char lower = (char) (97 + r.nextInt(26)); //ASCII码十进制代码97-122代表小写字母a-z
                pwlist.add(lower);
            } else {
                char lower = (char) (97 + r.nextInt(26)); //ASCII码十进制代码97-122代表小写字母a-z
                pwlist.add(lower);
            }
        }
        //乱序
        Collections.shuffle(pwlist);
        StringBuilder str = new StringBuilder();
        //将Char类型的List，循环遍历，最后以String类型输出
        /*第一种循环方式
        for(int i=0;i<pwlist.size();i++) {
            Character ch=pwlist.get(i);
            str.append(ch);
        }
        */
        /*第二种循环方式
        for(Character ch : pwlist){
            str.append(ch);
        }
        */
        //jdk8的forEach循环
        pwlist.forEach(ch -> {
            str.append(ch);
        });
        return str.toString();
    }



}
