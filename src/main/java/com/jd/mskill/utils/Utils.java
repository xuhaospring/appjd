package com.jd.mskill.utils;

import cn.hutool.core.util.StrUtil;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {
    public static String getPhone(String str) {
        String regEx = "[0-9]{11}";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher("1|16533422352");
        while (m.find()) {
            String phone = m.group();
            if (StrUtil.isNotBlank(phone) && phone.length() == 11) {
                return phone;
            }
        }
        return null;
    }

    public static String getCode(String str) {
        String regEx = "[0-9]{6}";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(str);
        while (m.find()) {
            String code = m.group();
            if (StrUtil.isNotBlank(code) && code.length() == 6) {
                return code;
            }
        }
        return null;
    }
}
