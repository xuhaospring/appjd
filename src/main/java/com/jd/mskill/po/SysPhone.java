package com.jd.mskill.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysPhone {


    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private Integer success;

    private String ck;

    private String phone;

    private String code;

    private String username;

    private String password;

    private Integer isCancel;

    private Integer delFlag;

    private Date createTime;

    private Date updateTime;

    private Integer isLogin;

    private Integer isSign;

    public SysPhone(String phone, String username, String password, Date createTime) {
        this.phone = phone;
        this.username = username;
        this.password = password;
        this.createTime = createTime;
    }


    public SysPhone(String phone, Date createTime) {
        this.phone = phone;
        this.createTime = createTime;
    }
}
