package com.jd.mskill.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysImg {

    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "日志编号")
    private Long id;

    //图片地址
    private String picMd5;

    //图片数据bash64
    private String imgData;

    //数据的坐标 具体可以查看x1y1x2y2这个对象的json
    private String x1y1;

    private Integer findCount;
    //时间
    private Date create_time;

    public SysImg(String picMd5, String imgData, String x1y1,Date create_time) {
        this.create_time = create_time;
        this.picMd5 = picMd5;
        this.imgData = imgData;
        this.x1y1 = x1y1;
    }



}
