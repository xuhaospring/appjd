package com.jd.mskill.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysToken {

    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private Integer status;

    private String token;
    //1|61b5a23f9b0b8a1b05bf62825828c47a|53.780|0.000
    private BigDecimal balance;

    private BigDecimal reflectBalance;

    private Integer delFlag;

    private Date createTime;

    public SysToken(Integer status, String token, BigDecimal balance, BigDecimal reflectBalance, Integer delFlag, Date createTime) {
        this.status = status;
        this.token = token;
        this.balance = balance;
        this.reflectBalance = reflectBalance;
        this.delFlag = delFlag;
        this.createTime = createTime;
    }
}
