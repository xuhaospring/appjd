
package com.jd.mskill.config;

/**
 * @author 徐浩
 * @date 2020-05-27
 */
public interface CommonConstants {

    /**
     * 成功标记
     */
    Integer SUCCESS = 0;
    /**
     * 失败标记
     */
    Integer FAIL = 1;

    Integer ONE=1;

    Integer ZERO=0;



}
