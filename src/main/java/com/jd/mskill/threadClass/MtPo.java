package com.jd.mskill.threadClass;


import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.util.Map;
import java.util.concurrent.Callable;


public class MtPo implements Callable<String> {

    private String body;
    public MtPo(String body) {
    this.body=body;

    }


    @Override
    public String call() throws Exception {
        JSONObject jsonObject = JSON.parseObject(this.body);

        HttpResponse execute = HttpRequest.post("https://mtjclfpt.com.cn/appapi/activitywineorder").body(this.body)
                .header("Authorization","bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsaWNlbnNlIjoibWFkZSBieSBtdCIsInBob25lIjoiMTg0MDgyODIyNDUiLCJ1c2VyX25hbWUiOiIxODQwODI4MjI0NSIsInNjb3BlIjpbInNlcnZlciJdLCJuaWNrbmFtZSI6InhoMTAxNSIsImV4cCI6MTYxNjcyMTMwNSwidXNlck5hbWUiOiIxODQwODI4MjI0NSIsInVzZXJJZCI6ImRjNWM5ZGJmNzI2MjQ3YTFiYzYyZWI5ZTdiNjZkOGM1IiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9VU0VSIl0sImp0aSI6IjZhN2E5M2E1LTE3YzUtNGNjNS1hZmRmLWEwZGY2YzM5MzY1YyIsImNsaWVudF9pZCI6ImFwcCJ9.uvEuy0LUkCRhfS_FZV32F2TWZyG6W0BebSlozYMMH5w")
                .contentType("application/json;charset=UTF-8").timeout(2000).execute();
            if(StrUtil.isNotBlank(execute.body())){
                return execute.body();
            }
        return null;
    }
}
