package com.jd.mskill.threadClass;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;
import java.util.Map;

@Slf4j
public class SubmitOrder implements Runnable {
    private String api_url;
    private String body;
    private String cookie;
    private String contentType;
    private String sku;
    private String usid;
    private Date buyTime;
    private Long mskill_time;

    public SubmitOrder(String bashUrl, String body, String cookie, String contentType, Map<String, String> params, String sku, String usid, Date buyTime, Long mskill_time) {
        StringBuilder paramsSb = new StringBuilder();
        if (params != null && params.size() > 0) {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                paramsSb.append("&").append(entry.getKey()).append("=").append(entry.getValue());
            }
        }
        String paramsStr = paramsSb.toString();
        this.api_url = bashUrl + paramsStr;
        this.body = body;
        this.cookie = cookie;
        this.contentType = contentType;
        this.sku = sku;
        this.usid = usid;
        this.buyTime = buyTime;
        this.mskill_time = mskill_time;
    }

    @Override
    public void run() {
        String jd_time_str = HttpUtil.get("https://a.jd.com//ajax/queryServerData.html");
        Long jd_time = Long.valueOf(JSON.parseObject(jd_time_str).get("serverTime").toString());
        if (jd_time - buyTime.getTime() > mskill_time * 1000L) {
            log.info("抢购时间结束:sku:{},时间为:{}", sku, new Date().toLocaleString());
            return;
        }
        try {
            log.info("等待订单提交为sku:{}", sku);
            jd_time_str = HttpUtil.get("https://a.jd.com//ajax/queryServerData.html");
            jd_time = Long.valueOf(JSON.parseObject(jd_time_str).get("serverTime").toString());
            HttpResponse execute = HttpRequest.post(api_url).form("body", body).form("usid", usid).cookie(cookie).contentType(contentType).timeout(2000).execute();
            int status = execute.getStatus();
            if (status == 200) {
                JSONObject jsonObject = JSON.parseObject(execute.body());
                if (jsonObject.containsKey("submitOrder")) {
                    Object submitOrder = jsonObject.get("submitOrder");
                    JSONObject submitOrderJSOn = JSON.parseObject(JSON.toJSONString(submitOrder));
                    if (submitOrderJSOn.containsKey("OrderId") && !submitOrderJSOn.get("OrderId").toString().equals("0")) {
                        log.info("当前线程提交下单成功 sku:{},内容为:{},订单号为：{}", sku, execute.body(), submitOrderJSOn.get("OrderId"));
                    } else {
                        log.info("当前线程提交失败 sku:{},内容为:{},订单号为：{}", sku, execute.body());
                    }
                }
            }
            Thread.sleep(1000L);
            if (jd_time - buyTime.getTime() > mskill_time * 1000) {
                log.info("抢购时间结束:sku:{}", sku);
            }
        } catch (Exception e) {
            log.error("未知错误，{},请联系耗子哥", e.toString());
        }
    }
}
