package com.jd.mskill.threadClass;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

@Slf4j
public class Appoint implements Runnable {

    private String api_url;
    private String body;
    private String cookie;
    private String contentType;
    private String api_url_1;

    public Appoint(String bashUrl, String body, String cookie, String contentType, Map<String, String> params) {
        StringBuilder paramsSb = new StringBuilder();
        if (params != null && params.size() > 0) {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                paramsSb.append("&").append(entry.getKey()).append("=").append(entry.getValue());
            }
        }
        String paramsStr = paramsSb.toString();
        this.api_url = bashUrl + paramsStr;
        this.body = body;
        this.cookie = cookie;
        this.contentType = contentType;
    }

    @Override
    public void run() {
        try {
            HttpResponse execute = HttpRequest.post(api_url).form("body", body).cookie(cookie).contentType(contentType).timeout(2000).execute();
            int status = execute.getStatus();
            if (status == 200) {
                JSONObject jsonObject = JSON.parseObject(execute.body());
                log.info("预约结果cookie:[{}] sku:[{}],",cookie.substring(0,20),body);
            }
        } catch (Exception e) {
            log.error("未知错误，{},请联系耗子哥", e.toString());
        }
    }
}
