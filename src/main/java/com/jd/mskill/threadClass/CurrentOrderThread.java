package com.jd.mskill.threadClass;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;
import java.util.Map;

@Slf4j
public class CurrentOrderThread implements Runnable {
    private String api_url;
    private String body;
    private String cookie;
    private String contentType;
    private String sku;
    private SubmitOrder submitOrder;
    private Date buyTime;
    private Long mskill_time;

    public CurrentOrderThread(String bashUrl, String body, String cookie, String contentType, Map<String, String> params, String sku, Date buyTime, SubmitOrder submitOrder, Long mskill_time) {
        StringBuilder paramsSb = new StringBuilder();
        if (params != null && params.size() > 0) {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                paramsSb.append("&").append(entry.getKey()).append("=").append(entry.getValue());
            }
        }
        String paramsStr = paramsSb.toString();
        this.api_url = bashUrl + paramsStr;
        this.body = body;
        this.cookie = cookie;
        this.contentType = contentType;
        this.sku = sku;
        this.submitOrder = submitOrder;
        this.buyTime = buyTime;
        this.mskill_time = mskill_time;
    }

    @Override
    public void run() {
        try {
            String jd_time_str = HttpUtil.get("https://a.jd.com//ajax/queryServerData.html");
            Long jd_time = Long.valueOf(JSON.parseObject(jd_time_str).get("serverTime").toString());
            long sleepTime = buyTime.getTime() - jd_time - 2000L;
            if (jd_time - buyTime.getTime() > mskill_time * 1000) {
                log.info("抢购时间结束:sku:{}", sku);
                return;
            }
            log.info("等待提交毫秒数为:{},sku:{}", sleepTime, sku);
            Thread.sleep(sleepTime);
            log.info("线程醒来了sku:{}", sku);
            jd_time_str = HttpUtil.get("https://a.jd.com//ajax/queryServerData.html");
            jd_time = Long.valueOf(JSON.parseObject(jd_time_str).get("serverTime").toString());
            long subTime = new Date().getTime() - jd_time;
            Integer index = 0;
            while ((new Date().getTime() - subTime) - buyTime.getTime() <= mskill_time * 1000) {
                HttpResponse execute = HttpRequest.post(api_url).form("body", body).cookie(cookie).contentType(contentType).timeout(2000).execute();
                int status = execute.getStatus();
                if (status == 200) {
                    if (index == 0) {
                        log.info("第一次提交");
                        new Thread(submitOrder).start();
                    } else {
                        index = index + 1;
                        Thread.sleep(5000L);
                        new Thread(submitOrder).start();
                    }
//                    log.info("提交订单信息成功:{}", StrUtil.isNotBlank(execute.body()) ? execute.body().substring(0, 15) : null);
                    log.info("提交订单信息成功:{}", execute.body());
                }
            }
            if (jd_time - buyTime.getTime() > mskill_time * 1000) {
                log.info("抢购时间结束:sku:{}", sku);
            }
        } catch (Exception e) {
            log.error("下单失败：sku:{},提及时间为:{},错误信息:{}", sku, new Date().toLocaleString(), e);
        }
    }
}