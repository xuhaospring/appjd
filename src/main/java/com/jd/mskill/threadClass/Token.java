package com.jd.mskill.threadClass;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import java.util.concurrent.Callable;

@Slf4j
public class Token implements Callable<String> {

    private String api_url;
    private String body;
    private String cookie;
    private String contentType;
    private String sku;

    public Token(String bashUrl, String body, String cookie, String contentType, Map<String, String> params, String sku) {
        StringBuilder paramsSb = new StringBuilder();
        if (params != null && params.size() > 0) {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                paramsSb.append("&").append(entry.getKey()).append("=").append(entry.getValue());
            }
        }
        this.sku = sku;
        String paramsStr = paramsSb.toString();
        this.api_url = bashUrl + paramsStr;
        this.body = body;
        this.cookie = cookie;
        this.contentType = contentType;
    }


    @Override
    public String call() {
        try {
            HttpResponse execute = HttpRequest.post(api_url).form("body", body).cookie(cookie).contentType(contentType).timeout(2000).execute();
            int status = execute.getStatus();
            if (status == 200) {
                JSONObject jsonObject = JSON.parseObject(execute.body());
                jsonObject.put("sku", sku);
                return JSON.toJSONString(jsonObject);
            }
        } catch (Exception e) {
            log.error("未知错误，{},请联系耗子哥", e.toString());
        }
        return null;
    }

    public static void main(String[] args) {

        String a = "{\"childActivityUrl\":\"openapp.jdmobile://virtual?params={\\\"category\\\":\\\"jump\\\",\\\"des\\\":\\\"couponCenter\\\"}\",\"eid\":\"eidAc3d9812113s7iFWFZ69OTPaUr1hRmFNcDohVht738bMw3RQku0oWIOBuSTcSgZr\",\"monitorRefer\":\"appClient\",\"monitorSource\":\"cc_sign_android_index_config\",\"pageClickKey\":\"Coupons_GetCenter\",\"shshshfpb\":\"j73v6QTWPDwkRyxz2m43ox1TyxAz4GH3Pe+PuUqsvIbj67eF3hqWHbM5RFsLYTYmA\"}";
        String s = JSON.toJSONString(a);
        System.out.println(s);
    }


}
