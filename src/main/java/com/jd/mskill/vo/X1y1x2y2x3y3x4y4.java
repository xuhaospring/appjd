package com.jd.mskill.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class X1y1x2y2x3y3x4y4 {

    private Integer x1;
    private Integer x2;
    private Integer x3;
    private Integer x4;

    private Integer y1;
    private Integer y2;
    private Integer y3;
    private Integer y4;


}
