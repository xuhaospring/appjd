package com.jd.mskill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jd.mskill.po.SysSmsAccount;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 日志表 Mapper 接口
 * </p>
 *
 * @author 徐浩
 * @date 2020-05-27
 */
@Mapper
public interface SysSmsAccountMapper extends BaseMapper<SysSmsAccount> {


}
