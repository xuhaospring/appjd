package com.jd.mskill.mapper;

import com.jd.mskill.po.SysImg;
import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 日志表 Mapper 接口
 * </p>
 *
 * @author 徐浩
 * @date 2020-05-27
 */
@Mapper
public interface SysImgMapper extends BaseMapper<SysImg> {


}
