package com.jd.mskill.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class SignDaily {

    private String uuid;
    private String currentOrderFunctionId;
    private String clientVersion;
    private String ck;
    private String skuParam;
}
