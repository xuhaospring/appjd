package com.jd.mskill.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Data
@NoArgsConstructor
@Slf4j
@AllArgsConstructor
@Component
public class ParamsDto {

    private String functionId;
    private String build;
    //一下3个是一样的
    private String uuid;
    private String openudid;
    private String aid;

    @Value("clientVersion")
    private String clientVersion;
    @Value("client")
    private String client;
    @Value("d_brand")
    private String d_brand;
    @Value("d_model")
    private String d_model;
    @Value("osVersion")
    private String osVersion;
    @Value("screen")
    private String screen;
    @Value("partner")
    private String partner;
    @Value("sdkVersion")
    private String sdkVersion;
    @Value("lang")
    private String lang;
    @Value("networkType")
    private String networkType;
    @Value("wifiBssid")
    private String wifiBssid;


}
