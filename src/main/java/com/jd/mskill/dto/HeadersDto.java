package com.jd.mskill.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Data
@NoArgsConstructor
@Slf4j
@AllArgsConstructor
@Component
public class HeadersDto {


    @Value("Connection")
    private String Connection;
    @Value("ConnectionValue")
    private String ConnectionValue;


    @Value("Cookie")
    private String Cookie;

    private String CookieValue;

    @Value("Accept_Encoding")
    private String Accept_Encoding;
    @Value("Accept_Encoding_Value")
    private String Accept_Encoding_Value;


    @Value("User_Agent")
    private String User_Agent;
    @Value("User_Agent_Value")
    private String User_Agent_Value;


    @Value("Content_Type")
    private String Content_Type;
    @Value("Content_Type_Value")
    private String Content_Type_Value;


    @Value("Host")
    private String Host;
    @Value("Host_Value")
    private String Host_Value;

    @Value("jdc_backup")
    private String jdc_backup;
    private String jdc_backup_Value;

}
