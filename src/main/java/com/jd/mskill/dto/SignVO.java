package com.jd.mskill.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SignVO {


    private String skuParam;
    private String currentOrderFunctionId;
    private String uuid;
    private String  clientVersion;

}
