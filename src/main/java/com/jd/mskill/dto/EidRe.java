package com.jd.mskill.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@NoArgsConstructor
@AllArgsConstructor
public class EidRe {

    private String st;
    private String sign;
    private String sv;
    private String body;
    private String uuid;
    private String version;
    private String function;


}
