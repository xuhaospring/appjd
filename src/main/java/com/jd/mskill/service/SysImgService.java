package com.jd.mskill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jd.mskill.po.SysImg;

/**
 * <p>
 * 日志表 服务类
 * </p>
 * @author 徐浩
 * @date 2020-05-27
 */
public interface SysImgService extends IService<SysImg> {



}
