package com.jd.mskill.service.impl;

import com.jd.mskill.mapper.SysImgMapper;
import com.jd.mskill.po.SysImg;
import com.jd.mskill.service.SysImgService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * <p>
 * 日志表 服务实现类
 * @author 徐浩
 * @date 2020-05-27
 */
@Service
public class SysImgServiceImpl extends ServiceImpl<SysImgMapper, SysImg> implements SysImgService {

}
